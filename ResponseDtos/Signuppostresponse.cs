namespace infinityrealmapi.ResponseDtos
{
    //anything in this folder and this class represents the external model we will
    //be sending back as a response to the client
    public class Signuppostresponse 
    {
        public string Id{get; set; }

        public string Fullname{get; set; }

        public string Username{get; set; }

        public string Email{get; set; }

        public string Phonenumber{get; set;}

    }
}