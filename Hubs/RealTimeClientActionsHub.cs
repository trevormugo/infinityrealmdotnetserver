using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;
using AutoMapper;
using infinityrealmapi.Repo;
using infinityrealmapi.ResponseDtos;
using infinityrealmapi.RequestDtos;
using infinityrealmapi.Model;
using System;
using System.Collections.Generic;

namespace infinityrealmapi.Hubs
{
    //route: api/realtimeclientactionshub?userId=userid       
    public class RealTimeClientActionsHub : Hub
    {

        private IRealTimeClientActionsInterface _service;

        private IUploadsinterface _uploadsservice;


        private IMapper _mapper;

        public RealTimeClientActionsHub(IRealTimeClientActionsInterface service, IUploadsinterface uploadsservice, IMapper mapper)
        {
            _service = service;
            _uploadsservice = uploadsservice;
            _mapper = mapper;
        }

        /************************************  
         ACCOUNT ACTIONS   
        *********************************/

        public async Task FollowAccount(FollowRequest followrequest)
        {
            var followingresponse = _service.FetchDoerAccount(followrequest.Account);
            var followedresponse = _service.FetchRecieverAccount(followrequest.AccountFollowed);
            if (followingresponse == null || followedresponse == null)
            {
                HashSet<string> connids = _service.FetchUserConnectionId(followrequest.Account);
                if (connids.Count == 0 || connids == null)
                {
                    Console.WriteLine("User is disconnected");
                    return;
                }

                foreach (var id in connids)
                {
                    await Clients.Client(id).SendAsync("Follow", "Account does not exist Error");
                }
            }
            else
            {
                var follow = _mapper.Map<Follows>(followrequest);
                var check = _service.FetchFollow(follow);
                if (check != null)
                {
                    HashSet<string> connids = _service.FetchUserConnectionId(followrequest.Account);
                    Console.WriteLine("already following");
                    if (connids.Count == 0 || connids == null)
                    {
                        Console.WriteLine("User is disconnected");
                        return;
                    }

                    foreach (var id in connids)
                    {
                        await Clients.Client(id).SendAsync("Follow", "Already following this account");
                    }
                }
                else
                {
                    var response = _service.FollowAccount(follow);
                    //save a notification object here  
                    Notifications notification = new Notifications
                    {
                        ToAccount = followrequest.AccountFollowed,

                        FromAccount = followrequest.Account,

                        FromAccountName = followingresponse.Username,

                        FromAccountThumbnail = followingresponse.Thumbnailpath,

                        NotificationType = "follow",

                        NotificationString = followingresponse.Username + "started following you"
                    };
                    _service.SaveNotificationObject(notification);
                    //send a push notification to their device here    

                    HashSet<string> connids = _service.FetchUserConnectionId(followrequest.AccountFollowed);
                    if (connids.Count == 0 || connids == null)
                    {
                        Console.WriteLine("User is disconnected");
                        return;
                    }
                    foreach (var id in connids)
                    {
                        await Clients.Client(id).SendAsync("Follow", followrequest);
                    }
                }
            }
        }


        public async Task UnfollowAccount(FollowRequest followrequest)
        {
            var followingresponse = _service.FetchDoerAccount(followrequest.Account);
            var followedresponse = _service.FetchRecieverAccount(followrequest.AccountFollowed);
            if (followingresponse == null || followedresponse == null)
            {
                HashSet<string> connids = _service.FetchUserConnectionId(followrequest.Account);
                if (connids.Count == 0 || connids == null)
                {
                    Console.WriteLine("User is disconnected");
                    return;
                }

                foreach (var id in connids)
                {
                    await Clients.Client(id).SendAsync("UnFollow", "Account does not exist Error");
                }
            }
            else
            {
                var follow = _mapper.Map<Follows>(followrequest);
                _service.UnFollowAccount(follow);
                //remove a notification object here  
                _service.RemoveNotificationObject(followrequest.AccountFollowed, followrequest.Account, "follow");
                HashSet<string> connids = _service.FetchUserConnectionId(followrequest.AccountFollowed);
                if (connids.Count == 0 || connids == null)
                {
                    Console.WriteLine("User is disconnected");
                    return;
                }
                foreach (var id in connids)
                {
                    await Clients.Client(id).SendAsync("UnFollow", followrequest);
                }
            }
        }


        /************************************  
         UPLOAD ACTIONS   
        *********************************/

        public async Task LikeUpload(LikeRequest likerequest)
        {
            var likedresponse = _service.FetchDoerAccount(likerequest.Account);
            var uploadresponse = _uploadsservice.fetchupload(likerequest.UploadLiked);
            if (likedresponse == null || uploadresponse == null)
            {
                //var upload = _service.FetchAccountIdByUploadId(likerequest.UploadLiked); _     
                HashSet<string> connids = _service.FetchUserConnectionId(likerequest.Account);
                if (connids.Count == 0 || connids == null)
                {
                    Console.WriteLine("User is disconnected");
                    return;
                }

                foreach (var id in connids)
                {
                    await Clients.Client(id).SendAsync("UnFollow", "Account does not exist Error");
                }
            }
            else
            {
                var like = _mapper.Map<Likes>(likerequest);
                _service.SaveLike(like);
                var upload = _service.FetchAccountIdByUploadId(likerequest.UploadLiked);
                Notifications notification = new Notifications
                {
                    ToAccount = upload.Uploader,

                    FromAccount = likedresponse.Id,

                    FromAccountName = likedresponse.Username,

                    FromAccountThumbnail = likedresponse.Thumbnailpath,

                    NotificationType = "like",

                    NotificationString = likedresponse.Username + "Liked an Upload"
                };
                _service.SaveNotificationObject(notification);
                HashSet<string> connids = _service.FetchUserConnectionId(upload.Uploader);
                if (connids.Count == 0 || connids == null)
                {
                    Console.WriteLine("User is disconnected");
                    return;
                }
                foreach (var id in connids)
                {
                    await Clients.Client(id).SendAsync("Like", likerequest);
                }

            }
        }


        public async Task UnlikeUpload(LikeRequest likerequest)
        {
            var likedresponse = _service.FetchDoerAccount(likerequest.Account);
            var uploadresponse = _uploadsservice.fetchupload(likerequest.UploadLiked);
            if (likedresponse == null || uploadresponse == null)
            {
                //var upload = _service.FetchAccountIdByUploadId(likerequest.UploadLiked); _     
                HashSet<string> connids = _service.FetchUserConnectionId(likerequest.Account);
                if (connids.Count == 0 || connids == null)
                {
                    Console.WriteLine("User is disconnected");
                    return;
                }

                foreach (var id in connids)
                {
                    await Clients.Client(id).SendAsync("UnFollow", "Account does not exist Error");
                }
            }
            else
            {
                _service.RemoveLike(likerequest);
                var upload = _service.FetchAccountIdByUploadId(likerequest.UploadLiked);

                _service.RemoveNotificationObject(upload.Uploader, likedresponse.Id, "like");
                HashSet<string> connids = _service.FetchUserConnectionId(upload.Uploader);
                if (connids.Count == 0 || connids == null)
                {
                    Console.WriteLine("User is disconnected");
                    return;
                }
                foreach (var id in connids)
                {
                    await Clients.Client(id).SendAsync("UnLike", likerequest);
                }

            }
        }


        /************************************  
         MAP ACTIONS   
        *********************************/

        public async Task PlaceMarkerObjectAtCoordinate(MarkerRequest markerrequest)
        {
            var account = _service.FetchDoerAccount(markerrequest.Account);
            if (account == null)
            {
                HashSet<string> connids = _service.FetchUserConnectionId(markerrequest.Account);
                if (connids.Count == 0 || connids == null)
                {
                    Console.WriteLine("User is disconnected");
                    return;
                }

                foreach (var id in connids)
                {
                    await Clients.Client(id).SendAsync("AddMarker", "Youre Account does not exist Error");
                }
            }
            else
            {
                var marker = _mapper.Map<Markers>(markerrequest);
                _service.AddMarker(marker);
                List<Followers> followers = _service.FetchFollowers(account.Id);
                foreach (var follower in followers)
                {
                    Notifications notification = new Notifications
                    {
                        ToAccount = follower.account,

                        FromAccount = follower.accountfollowed,

                        FromAccountName = follower.Accounts[0].user,

                        FromAccountThumbnail = follower.Accounts[0].thumbnailpath,

                        NotificationType = "marker",

                        NotificationString = follower.Accounts[0].user + "Placed a marker on the map"
                    };
                    _service.SaveNotificationObject(notification);
                    HashSet<string> connids = _service.FetchUserConnectionId(follower.account);
                    if (connids.Count == 0 || connids == null)
                    {
                        Console.WriteLine("User is disconnected");
                        return;
                    }
                    foreach (var id in connids)
                    {
                        await Clients.Client(id).SendAsync("AddMarker", marker);
                    }
                }

            }

        }


        public async Task RemoveMarkerObjectAtCoordinate(MarkerRequest markerrequest)
        {
            var markerobj = _service.FetchMarker(markerrequest);
            if (markerobj == null)
            {
                HashSet<string> connids = _service.FetchUserConnectionId(markerrequest.Account);
                if (connids.Count == 0 || connids == null)
                {
                    Console.WriteLine("User is disconnected");
                    return;
                }

                foreach (var id in connids)
                {
                    await Clients.Client(id).SendAsync("Marker", "Marker is not on database");
                }
            }
            else
            {
                _service.RemoveMarker(markerobj.Id);
                List<Followers> followers = _service.FetchFollowers(markerobj.Account);
                foreach (var follower in followers)
                {
                    _service.RemoveNotificationObject(follower.account, follower.accountfollowed, "marker");
                    HashSet<string> connids = _service.FetchUserConnectionId(follower.account);
                    if (connids.Count == 0 || connids == null)
                    {
                        Console.WriteLine("User is disconnected");
                        return;
                    }
                    foreach (var id in connids)
                    {
                        await Clients.Client(id).SendAsync("RemoveMarker", markerrequest);
                    }
                }

            }

        }


        public async Task AddRsvpToMarker(MarkerRSVPRequest request)
        {
            var marker = _service.FetchMarkerById(request.Marker);
            if (marker == null)
            {
                HashSet<string> connids = _service.FetchUserConnectionId(request.Account);
                if (connids.Count == 0 || connids == null)
                {
                    Console.WriteLine("User is disconnected");
                    return;
                }

                foreach (var id in connids)
                {
                    await Clients.Client(id).SendAsync("AddMarkerrsvp", "Marker is not on database");
                }
            }
            else
            {
                var rsvp = _mapper.Map<MarkerRSVP>(request);
                _service.AddRSVPToMarker(rsvp);
                List<Followers> followers = _service.FetchFollowers(request.Account);
                foreach (var follower in followers)
                {
                    Notifications notification = new Notifications
                    {
                        ToAccount = follower.account,

                        FromAccount = follower.accountfollowed,

                        FromAccountName = follower.Accounts[0].user,

                        FromAccountThumbnail = follower.Accounts[0].thumbnailpath,

                        NotificationType = "markerrsvp",

                        NotificationString = follower.Accounts[0].user + "RSVPied to a marker event"
                    };
                    _service.SaveNotificationObject(notification);
                    HashSet<string> connids = _service.FetchUserConnectionId(follower.account);
                    if (connids.Count == 0 || connids == null)
                    {
                        Console.WriteLine("User is disconnected");
                        return;
                    }
                    foreach (var id in connids)
                    {
                        await Clients.Client(id).SendAsync("AddMarkerrsvp", request);
                    }
                }

            }

        }


        public async Task RemoveRsvpToMarker(MarkerRSVPRequest request)
        {
            var marker = _service.FetchMarkerById(request.Marker);
            if (marker == null)
            {
                HashSet<string> connids = _service.FetchUserConnectionId(request.Account);
                if (connids.Count == 0 || connids == null)
                {
                    Console.WriteLine("User is disconnected");
                    return;
                }

                foreach (var id in connids)
                {
                    await Clients.Client(id).SendAsync("RemoveMarkerrsvp", "Marker is not on database");
                }
            }
            else
            {
                _service.RemoveRSVPFromMarker(request.Marker, request.Account);
                List<Followers> followers = _service.FetchFollowers(request.Account);
                foreach (var follower in followers)
                {
                    _service.RemoveNotificationObject(follower.account, follower.accountfollowed, "markerrsvp");
                    HashSet<string> connids = _service.FetchUserConnectionId(follower.account);
                    if (connids.Count == 0 || connids == null)
                    {
                        Console.WriteLine("User is disconnected");
                        return;
                    }
                    foreach (var id in connids)
                    {
                        await Clients.Client(id).SendAsync("RemoveMarkerrsvp", request);
                    }
                }

            }
        }



        /************************************  
         CHAT ACTIONS   
        *********************************/
        public async Task SendMessageToAccount(ChatRequest request)
        {
            var fromaccount = _service.FetchDoerAccount(request.Account);
            var toaccount = _service.FetchRecieverAccount(request.ToAccount);
            if (fromaccount == null || toaccount == null)
            {
                HashSet<string> connids = _service.FetchUserConnectionId(request.Account);
                if (connids.Count == 0 || connids == null)
                {
                    Console.WriteLine("User is disconnected");
                    return;
                }

                foreach (var id in connids)
                {
                    await Clients.Client(id).SendAsync("Chats", "Account doesnt exist");
                }
            }
            else
            {
                var chat = _mapper.Map<Chats>(request);
                _service.SendChat(chat);
                Notifications notification = new Notifications
                {
                    ToAccount = toaccount.Id,

                    FromAccount = fromaccount.Id,

                    FromAccountName = fromaccount.Username,

                    FromAccountThumbnail = fromaccount.Thumbnailpath,

                    NotificationType = "chats",

                    NotificationString = fromaccount.Username + "sent a message " + chat.ChatMessage
                };
                _service.SaveNotificationObject(notification);
                HashSet<string> connids = _service.FetchUserConnectionId(toaccount.Id);
                if (connids.Count == 0 || connids == null)
                {
                    Console.WriteLine("User is disconnected");
                    return;
                }
                foreach (var id in connids)
                {
                    await Clients.Client(id).SendAsync("chats", request , notification);
                }


            }

        }

        public async Task UnsendMessageToAccount(ChatRequest request, string chatid)
        {
            var fromaccount = _service.FetchDoerAccount(request.Account);
            var toaccount = _service.FetchRecieverAccount(request.ToAccount);
            if (fromaccount == null || toaccount == null)
            {
                HashSet<string> connids = _service.FetchUserConnectionId(request.Account);
                if (connids.Count == 0 || connids == null)
                {
                    Console.WriteLine("User is disconnected");
                    return;
                }

                foreach (var id in connids)
                {
                    await Clients.Client(id).SendAsync("Unsendchat", "Account doesnt exist");
                }
            }
            else
            {
                _service.UnsendChat(chatid);
                _service.RemoveNotificationObject(request.Account, request.ToAccount, "chats");
                HashSet<string> connids = _service.FetchUserConnectionId(toaccount.Id);
                if (connids.Count == 0 || connids == null)
                {
                    Console.WriteLine("User is disconnected");
                    return;
                }
                foreach (var id in connids)
                {
                    await Clients.Client(id).SendAsync("Unsendchat", request);
                }


            }


        }


        public async Task ReplyToMessage(ChatRepliesRequest replyrequest)
        {
            var fromaccount = _service.FetchDoerAccount(replyrequest.Account);
            var toaccount = _service.FetchRecieverAccount(replyrequest.ToAccount);
            if (fromaccount == null || toaccount == null)
            {
                HashSet<string> connids = _service.FetchUserConnectionId(replyrequest.Account);
                if (connids.Count == 0 || connids == null)
                {
                    Console.WriteLine("User is disconnected");
                    return;
                }

                foreach (var id in connids)
                {
                    await Clients.Client(id).SendAsync("ReplyChat", "Account doesnt exist");
                }
            }
            else
            {
                var reply = _mapper.Map<ChatReplies>(replyrequest);
                _service.ReplyToChat(reply);
                Notifications notification = new Notifications
                {
                    ToAccount = toaccount.Id,

                    FromAccount = fromaccount.Id,

                    FromAccountName = fromaccount.Username,

                    FromAccountThumbnail = fromaccount.Thumbnailpath,

                    NotificationType = "chats",

                    NotificationString = fromaccount.Username + "sent a message " + reply.ReplyChatMessage
                };
                _service.SaveNotificationObject(notification);

                HashSet<string> connids = _service.FetchUserConnectionId(toaccount.Id);
                if (connids.Count == 0 || connids == null)
                {
                    Console.WriteLine("User is disconnected");
                    return;
                }
                foreach (var id in connids)
                {
                    await Clients.Client(id).SendAsync("ReplyChat", replyrequest);
                }


            }

        }

        public async Task ForwardMessageToAccount(ChatForwardRequest request)
        {
            var fromaccount = _service.FetchDoerAccount(request.Account);
            if (fromaccount == null)
            {
                HashSet<string> connids = _service.FetchUserConnectionId(request.Account);
                if (connids.Count == 0 || connids == null)
                {
                    Console.WriteLine("User is disconnected");
                    return;
                }

                foreach (var id in connids)
                {
                    await Clients.Client(id).SendAsync("Forward", "Account doesnt exist");
                }
            }
            else
            {
                var accounts = _mapper.Map<ChatForwardRequest>(request);
                _service.ForwardChat(accounts);
                var message = _service.FetchChat(request.ChatId); 
                foreach (var account in accounts.ToAccounts)
                {
                    Notifications notification = new Notifications
                    {
                        ToAccount = account,

                        FromAccount = fromaccount.Id,

                        FromAccountName = fromaccount.Username,

                        FromAccountThumbnail = fromaccount.Thumbnailpath,

                        NotificationType = "chats",

                        NotificationString =fromaccount.Username + " Sent a message " + message.ChatMessage
                    };
                    _service.SaveNotificationObject(notification);
                    HashSet<string> connids = _service.FetchUserConnectionId(account);
                    if (connids.Count == 0 || connids == null)
                    {
                        Console.WriteLine("User is disconnected");
                        return;
                    }
                    foreach (var id in connids)
                    {
                        await Clients.Client(id).SendAsync("Forward",  request);
                    }
                }


            }

        }


        /************************************  
         CONNECTION HANDLER    
        *********************************/

        public override Task OnConnectedAsync()
        {
            //A new user connected    
            //add his map of userid to connid to a storage in memory 
            var httpContext = this.Context.GetHttpContext();
            var userId = httpContext.Request.Query["userId"];
            var connecionid = Context.ConnectionId;
            _service.AddUserToStorage(userId, connecionid);
            return base.OnConnectedAsync();
        }


        public override Task OnDisconnectedAsync(Exception exception)
        {
            //A user disconnected    
            //remove his map of userid to connid from a storage in memory 
            var httpContext = this.Context.GetHttpContext();
            var userId = httpContext.Request.Query["userId"];
            var connecionid = Context.ConnectionId;
            _service.RemoveUserFromStorage(userId, connecionid);
            return base.OnDisconnectedAsync(exception);
        }


    }
}



















