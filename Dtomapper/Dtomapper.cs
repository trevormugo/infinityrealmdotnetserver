using AutoMapper;
using infinityrealmapi.Model;
using infinityrealmapi.RequestDtos;
using infinityrealmapi.ResponseDtos;

namespace infinityrealapi.Dtomapper
{
    public class Dtomapper : Profile
    {
        public Dtomapper()
        {
            //route: api/useraccounts/signup
            CreateMap<Signuppost , Signuppostresponse>();
            CreateMap<Signuppostrequest , Signuppost>(); 

            //route: api/verification
            CreateMap<Sendemailrequest , VerifiedAccounts>();
            CreateMap<VerifiedAccounts , Sendemailresponse>();

            //route: api/useraccounts/login  
            CreateMap<Signuppost , Loginresponse>();

            //route: api/useraccounts/resetpassword 
            CreateMap<PasswordResetRequest , Signuppost>();

            //route: api/uploads/musicfileupload 
            CreateMap<Uploadmusicrequest , Uploads>();

            //route: api/uploads/createcuratedobject
            CreateMap<Createcuratedobj , Curatedobject>();

 
            //route: api/uploads/insertintocuratedobj 
            CreateMap<Curatedsavereqest , Uploads>();
            CreateMap<Curatedsavereqest , Curatedobjsaves>();

            //route: api/uploads/playliststream 
            CreateMap<PlaylistStreamRequest , PlaylistStreams>();

            //route: api/realtimeclientactionshub?userId=userid       
            CreateMap<FollowRequest , Follows>();
            CreateMap<MarkerRequest , Markers>();
            CreateMap<LikeRequest , Likes>();
            CreateMap<MarkerRSVPRequest , MarkerRSVP>();
            CreateMap<ChatRequest , Chats>();
            CreateMap<ChatLikeRequest , ChatLikes>();
            CreateMap<ChatRepliesRequest , ChatReplies>();



        }    
    }
}