using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using infinityrealmapi.Repo;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using infinityrealmapi.Hubs;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;

namespace infinityrealmapi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddSingleton<IMongoClient, MongoClient>(s =>
           {
               var uri = s.GetRequiredService<IConfiguration>()["MongoUri"];
               return new MongoClient(uri);
           });
            services.AddSignalR();
            services.AddScoped<IUseraccountsinterface, Useraccountsimplement>();
            services.AddScoped<IVerificationinterface, Verificationimplement>();
            services.AddScoped<IAccountpoolinterface, Accountpoolimplement>();
            services.AddScoped<IUploadsinterface, Uploadsimplement>();
            services.AddScoped<IRealTimeClientActionsInterface, RealTimeClientActionsImplement>();
            services.AddScoped<IMapsinterface, Mapsimplement>();
            services.AddScoped<IArtistinterface, ArtistImplement>();

            services.AddControllers();
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            //disabled for now  
            //app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHub<RealTimeClientActionsHub>("/api/realtimeclientactionshub");
            });
        }
    }
}
public class BlogIdGenerator : IIdGenerator
   {
 
       public object GenerateId(object container, object document)
       {
 
           return "Blog_" + Guid.NewGuid().ToString();
       }
 
       public bool IsEmpty(object id)
       {
           return id == null || String.IsNullOrEmpty(id.ToString());
       }
   }