using System.Collections.Generic;
using System.Threading.Tasks;
using infinityrealmapi.RequestDtos;
using infinityrealmapi.ResponseDtos;
using Square.Models;

namespace infinityrealmapi.Repo
{

    public interface IMapsinterface
    {
        List<MapsFollowing> fetchmarkers(string id);
    }
}