using infinityrealmapi.Model;
using infinityrealmapi.RequestDtos;

namespace infinityrealmapi.Repo
{
    public interface IVerificationinterface
    {
        bool sendcode(VerifiedAccounts emailobj , bool insertobj); 

        VerifiedAccounts fetchbyid(string id);   

        VerifiedAccounts fetchbyemail(string email);   

        //VerifiedAccounts fetchverificationbyemail(string email);

        VerifiedAccounts verifycode(string email , int code);

        void deleterequest(string email);
    }
}