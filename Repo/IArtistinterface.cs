using System.Collections.Generic;
using System.Threading.Tasks;
using infinityrealmapi.RequestDtos;
using infinityrealmapi.ResponseDtos;
using Square.Models;
using infinityrealmapi.Model;

namespace infinityrealmapi.Repo
{

    public interface IArtistinterface
    {
        List<Uploads> fetchuploadbyartistid(string id);

        List<Artists> fetchfollowedartists(string id);


    }
}