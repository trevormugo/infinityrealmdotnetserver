using System.Threading.Tasks;
using infinityrealmapi.RequestDtos;
using infinityrealmapi.ResponseDtos;
using infinityrealmapi.Model;
using System.Collections.Generic;

namespace infinityrealmapi.Repo
{
    public interface IRealTimeClientActionsInterface
    {

        void AddUserToStorage(string userid, string connectionid);

        void RemoveUserFromStorage(string userid, string connectionid);

        HashSet<string> FetchUserConnectionId(string userid);

        Signuppost FetchDoerAccount(string id);

        Signuppost FetchRecieverAccount(string id);

        Follows FollowAccount(Follows request);

        void UnFollowAccount(Follows request);

        void SaveNotificationObject(Notifications notification);

        void RemoveNotificationObject(string toaccount, string fromaccount, string type);

        Follows FetchFollow(Follows request);

        void SendPushNotification();

        void SaveLike(Likes request);

        void RemoveLike(LikeRequest request);

        Likes FetchLike(LikeRequest request);

        void AddMarker(Markers request);

        void RemoveMarker(string id);

        Markers FetchMarker(MarkerRequest request);

        Markers FetchMarkerById(string id);

        void AddRSVPToMarker(MarkerRSVP request);

        MarkerRSVP FetchRSVP(MarkerRSVPRequest request); 

        void RemoveRSVPFromMarker(string markerid, string accountid);    

        void SendChat(Chats request);

        void UnsendChat(string id);

        void LikeChat(ChatLikes request);

        void UnlikeChat(string id);

        void ReplyToChat(ChatReplies request);

        void DeleteReply(string id);

        void ForwardChat(ChatForwardRequest request);

        Chats FetchChat(string id); 

        Uploads FetchAccountIdByUploadId(string id);    
 
        List<Followers> FetchFollowers(string id); 

    }
}