using infinityrealmapi.Model;
using infinityrealmapi.RequestDtos;
using MongoDB.Driver;
namespace infinityrealmapi.Repo
{
    public class Useraccountsimplement : IUseraccountsinterface
    {
        //this class is only about the collection below
        private IMongoCollection<Signuppost> _clientdetails; 

        public Useraccountsimplement(IMongoClient client)
        {
          var database = client.GetDatabase("infinityrealm");
           _clientdetails = database.GetCollection<Signuppost>("clientdetails");
        }

        public Signuppost fetchaccount(string id)
        {
          var user = _clientdetails.Find<Signuppost>(signuppost => id == signuppost.Id).FirstOrDefault();
          return user;
        }

        public Signuppost fetchaccountbyemail(string email)
        {
          var user = _clientdetails.Find<Signuppost>(signuppost => email == signuppost.Email).FirstOrDefault();
          return user; 
        }

        public bool passwordreset(Signuppost resetbody)
        {
            var response =  _clientdetails.ReplaceOne<Signuppost>(model => model.Email == resetbody.Email ,resetbody );
            if(response != null)
            {
               return true;       
            }
            else
            {
              return false;
            }
        }

        public Signuppost signup(Signuppost account)
        {
          _clientdetails.InsertOne(account);
          return account;
        }

        public Signuppost userlogin(Loginrequest credentials)
        {
          var acc = _clientdetails.Find<Signuppost>(s => (credentials.Email == s.Email) && (credentials.Password == s.Password)).FirstOrDefault();
          return acc;  
        }
    }
}