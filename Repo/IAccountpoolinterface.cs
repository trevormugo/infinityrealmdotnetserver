using System.Threading.Tasks;
using infinityrealmapi.RequestDtos;
using infinityrealmapi.ResponseDtos;
using Square.Models;

namespace infinityrealmapi.Repo
{

   public interface IAccountpoolinterface
   {
        Task<RetrieveCustomerResponse> fetchcustomerbyid(string id);
 
        Task<SearchCustomersResponse> fetchcustomerbyrefid(string refid, string email); 

        Task<CreateCustomerResponse> createcustomer(Createcustomerrequest body);
   
        Task<CreateCustomerCardResponse> createcustomercard(Createcardrequest body);    
   
        Task<SearchCustomersResponse> retrievecustomercard(string refid , string phonenumber , string email); 

        Task<CreateSubscriptionResponse> createsubscription(CreateSubscriptionRequestInternal subscriptionbody);

        Task<CancelSubscriptionResponse> cancelsubscription(string subscriptionId);
    
        Task<RetrieveSubscriptionResponse> retrievesubscription(string subscriptionid);  

        Task<SearchSubscriptionsResponse> retrievesubscriptionbyrefid(string customerid);

        //the client is the masterapp from here 
        Task<UpsertCatalogObjectResponse> createsubscriptionplan(Createsubscriptionplanrequest planbody);    

        Task<RetrieveCatalogObjectResponse> retrievespecificsubscriptionplan(string objectid);      
       
        Task<SearchCatalogObjectsResponse> searchspecificsubscriptionplan(string planname);      

        Task<ListCatalogResponse> listsubscriptionplans(); 

        Task<DeleteCatalogObjectResponse> deletesubscriptionplan(string objectid); 

        Task<UpsertCatalogObjectResponse> updatesubscriptionplan(Updatesubscriptionplanrequest planbody);  

    }   
}