using System;
using System.Collections.Generic;
using infinityrealmapi.Model;
using infinityrealmapi.RequestDtos;
using MongoDB.Bson;
using MongoDB.Driver;
namespace infinityrealmapi.Repo
{
    public class Mapsimplement : IMapsinterface
    {
        //this class is only about the collection below
        private IMongoCollection<Markers> _markers;

        private IMongoCollection<Signuppost> _details;

        private IMongoCollection<Follows> _follows;

        public Mapsimplement(IMongoClient client)
        {
            var database = client.GetDatabase("infinityrealm");
            _markers = database.GetCollection<Markers>("markers");
            _details = database.GetCollection<Signuppost>("clientdetails");
            _follows = database.GetCollection<Follows>("follows");

        }

        public List<MapsFollowing> fetchmarkers(string id)
        {
            var following = _follows.Aggregate()
                              .Match(account => account.Account == id)
                              .Lookup(
                                  foreignCollection: _details,
                                  localField: x => x.Account,
                                  foreignField: y => y.Id,
                                  @as: (MapsFollowing following) => following.Accounts
                                )
                                .Lookup(
                                  foreignCollection: _markers,
                                  localField: x => x.accountfollowed,
                                  foreignField: y => y.Account,
                                  @as: (MapsFollowing follower) => follower.Accounts[0].marker
                              )
                              .ToList();
            Console.WriteLine(following);
            foreach (var i in following)
            {
                Console.WriteLine(i.ToJson());
            }
            return following;
        }


    }
}

//lookup : following
public class MapsFollowing
{
    public string account { get; set; }

    public string accountfollowed { get; set; }

    public int timestamp { get; set; }

    public List<FollowerAccountsforMaps> Accounts { get; set; }
}

public class FollowerAccountsforMaps
{
    public ObjectId Id { get; set; }

    public string name { get; set; }

    public string user { get; set; }

    public string email { get; set; }

    public string phonenumber { get; set; }

    public string thumbnailpath { get; set; }

    public long thumbnailsize { get; set; }

    public string thumbnailmimetype { get; set; }

    public string thumbnailext { get; set; }

    public string password { get; set; }

    public int timestamp { get; set; }


    public List<MarkerforAccount> marker { get; set; }

}
public class MarkerforAccount
{
    public ObjectId Id { get; set; }

    public ObjectId Account { get; set; }

    public string longitude { get; set; }

    public string latitude { get; set; }

    public int views { get; set; } = 0;

    public int timeusermarkedapoint { get; set; } = Convert.ToInt32((int)DateTime.Now.Subtract(new DateTime(1970, 1, 1)).TotalSeconds);

}