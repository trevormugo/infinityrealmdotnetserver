using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using infinityrealmapi.Model;
using infinityrealmapi.RequestDtos;
using Microsoft.AspNetCore.Http;
using MongoDB.Bson;
using MongoDB.Driver;
namespace infinityrealmapi.Repo
{
    public class Uploadsimplement : IUploadsinterface
    {
        //this class is only about the collection below
        private IMongoCollection<Uploads> _uploads;
        private IMongoCollection<Curatedobject> _playlistcurations;
        private IMongoCollection<Curatedobject> _albumcurations;
        private IMongoCollection<Curatedobjsaves> _playlistsavecurations;
        private IMongoCollection<Curatedobjsaves> _albumsavecurations;

        private IMongoCollection<PlaylistStreams> _playliststreams;
        private IMongoCollection<AlbumnStreams> _albumnstreams;
        private IMongoCollection<Streams> _singlestreams;

        public Uploadsimplement(IMongoClient client)
        {
            var database = client.GetDatabase("infinityrealm");
            _uploads = database.GetCollection<Uploads>("uploads");
            _playlistcurations = database.GetCollection<Curatedobject>("playlists");
            _albumcurations = database.GetCollection<Curatedobject>("albums");
            _playlistsavecurations = database.GetCollection<Curatedobjsaves>("playlistssave");
            _albumsavecurations = database.GetCollection<Curatedobjsaves>("albumssave");
            _playliststreams = database.GetCollection<PlaylistStreams>("playliststreams");
            _albumnstreams = database.GetCollection<AlbumnStreams>("albumnstreams");
            _singlestreams = database.GetCollection<Streams>("singlestreams");


        }

        public Uploads savetodb(Uploads data, IFormFile thumbnail, IFormFile musicfile, string thubnailext, string musicfileext, string thumbnailpath, string musicpath)
        {
            data.Thumbnailpath = thumbnailpath;
            data.Thumbnailsize = thumbnail.Length;
            data.Thumbnailmimetype = thumbnail.ContentType;
            data.Thumbnailext = thubnailext;
            data.Musicfilepath = musicpath;
            data.Musicfilesize = musicfile.Length;
            data.Musicfilemimetype = musicfile.ContentType;
            data.Musicfileext = musicfileext;
            _uploads.InsertOne(data);
            return data;
        }


        public Curatedobject storeobjindb(Curatedobject data, IFormFile thumbnail, string thumbnailpath, string type)
        {
            data.Pathtothumbnail = thumbnailpath;
            data.Thumbnailmimetype = thumbnail.ContentType;
            if (type == "album")
            {
                _albumcurations.InsertOne(data);
                return data;
            }
            else if (type == "playlist")
            {
                _playlistcurations.InsertOne(data);
                return data;
            }
            else
            {
                return null;
            }
        }


        public Curatedobjsaves saveincuration(string uploadid, Curatedobjsaves data, string type)
        {
            data.Upload = uploadid;
            if (type == "album")
            {
                _albumsavecurations.InsertOne(data);
                return data;
            }
            else if (type == "playlist")
            {
                _playlistsavecurations.InsertOne(data);
                return data;
            }
            else
            {
                return null;
            }
        }


        public Uploads fetchupload(string id)
        {
            var upload = _uploads.Find<Uploads>(upload => id == upload.Id).FirstOrDefault();
            return upload;
        }

        public Curatedobject fetchcuratedobject(string id, string type)
        {
            if (type == "album")
            {
                var upload = _albumcurations.Find<Curatedobject>(curatedobj => id == curatedobj.Id).FirstOrDefault();
                return upload;
            }
            else if (type == "playlist")
            {
                var upload = _playlistcurations.Find<Curatedobject>(curatedobj => id == curatedobj.Id).FirstOrDefault();
                return upload;
            }
            else
            {
                return null;
            }
        }


        public Curatedobjsaves fetchcurationsave(string id, string type)
        {
            if (type == "album")
            {
                var upload = _albumsavecurations.Find<Curatedobjsaves>(curatedobj => id == curatedobj.Id).FirstOrDefault();
                return upload;
            }
            else if (type == "playlist")
            {
                var upload = _playlistsavecurations.Find<Curatedobjsaves>(curatedobj => id == curatedobj.Id).FirstOrDefault();
                return upload;
            }
            else
            {
                return null;
            }
        }

        //Playlists  
        public PlaylistStreams insertplayliststream(PlaylistStreams request)
        {
            _playliststreams.InsertOne(request);
            return request;
        }

        public PlaylistStreams updateplayliststream(PlaylistStreamRequest request, PlaylistStreams streamobj)
        {
            streamobj.Number = streamobj.Number + 1;
            _playliststreams.ReplaceOne<PlaylistStreams>(model => model.Account == request.Account && model.PlaylistStreamed == request.PlaylistStreamed, streamobj);
            return streamobj;
        }

        public PlaylistStreams fetchplayliststreambycredentials(PlaylistStreamRequest request)
        {
            var response = _playliststreams.Find<PlaylistStreams>(model => model.Account == request.Account && model.PlaylistStreamed == request.PlaylistStreamed).FirstOrDefault();
            return response;
        }

        public PlaylistStreams fetchplayliststreambyid(string id)
        {
            var response = _playliststreams.Find<PlaylistStreams>(model => model.Id == id).FirstOrDefault();
            return response;
        }

        //Albumns 
        public AlbumnStreams insertalbumnstream(AlbumnStreams request)
        {
            _albumnstreams.InsertOne(request);
            return request;
        }

        public AlbumnStreams updatealbumnstream(AlbumnStreamRequest request, AlbumnStreams streamobj)
        {
            streamobj.Number = streamobj.Number + 1;
            _albumnstreams.ReplaceOne<AlbumnStreams>(model => model.Account == request.Account && model.AlbumnStreamed == request.AlbumnStreamed, streamobj);
            return streamobj;
        }

        public AlbumnStreams fetchalbumnstreambycredentials(AlbumnStreamRequest request)
        {
            var response = _albumnstreams.Find<AlbumnStreams>(model => model.Account == request.Account && model.AlbumnStreamed == request.AlbumnStreamed).FirstOrDefault();
            return response;
        }

        public AlbumnStreams fetchalbumnstreambyid(string id)
        {
            var response = _albumnstreams.Find<AlbumnStreams>(model => model.Id == id).FirstOrDefault();
            return response;
        }

        //Singles 
        public Streams insertsinglestream(Streams request)
        {
            _singlestreams.InsertOne(request);
            return request;
        }

        public Streams updatesinglestream(StreamRequest request, Streams streamobj)
        {
            streamobj.Number = streamobj.Number + 1;
            _singlestreams.ReplaceOne<Streams>(model => model.Account == request.Account && model.UploadStreamed == request.SingleStreamed, streamobj);
            return streamobj;
        }

        public Streams fetchsinglestreambycredentials(StreamRequest request)
        {
            var response = _singlestreams.Find<Streams>(model => model.Account == request.Account && model.UploadStreamed == request.SingleStreamed).FirstOrDefault();
            return response;
        }

        public Streams fetchsinglestreambyid(string id)
        {
            var response = _singlestreams.Find<Streams>(model => model.Id == id).FirstOrDefault();
            return response;
        }




        //Top 100 Singles  
        public List<Uploadstream> fetchtop100singles()
        {
            var singlestreams = _singlestreams.Aggregate()
                            .Group(
                                key => key.UploadStreamed,
                                streams => new { groupedobjectid = streams.Key, noofstreams = streams.Sum(no => no.Number) }
                            )
                            .SortBy(t => t.noofstreams)
                            .Limit(100)
                            .Lookup(
                               foreignCollection: _uploads,
                               localField: x => x.groupedobjectid,
                               foreignField: (Uploads single) => single.Id,
                               @as: (Uploadstream singles) => singles.uploads
                            )
                            .ToList();
            Console.WriteLine(singlestreams);
            foreach (var i in singlestreams)
            {
                Console.WriteLine(i.ToJson());
            }
            return singlestreams;

        }


        //Top 100 Singles  : REGION
        public List<Uploadstream> fetchtop100singlesregionfilter(string region)
        {
            var singlestreams = _singlestreams.Aggregate()
                            //                        .Match(region => region.Region.Equals(region) ) 
                            .Group(
                                key => key.UploadStreamed,
                                streams => new { groupedobjectid = streams.Key, noofstreams = streams.Sum(no => no.Number) }
                            )
                            .SortBy(t => t.noofstreams)
                            .Limit(100)
                            .Lookup(
                               foreignCollection: _uploads,
                               localField: x => x.groupedobjectid,
                               foreignField: (Uploads single) => single.Id,
                               @as: (Uploadstream singles) => singles.uploads
                            )
                            .ToList();
            Console.WriteLine(singlestreams);
            foreach (var i in singlestreams)
            {
                Console.WriteLine(i.ToJson());
            }
            return singlestreams;


        }


        //Top 100 Singles  : TRENDING
        public List<Uploadstream> fetchtop100singlestimestampfilter(int timestamp)
        {
            var singlestreams = _singlestreams.Aggregate()
                            //                            .Match(time => int.Parse(time.Timestamp) > timestamp)
                            .Group(
                                key => key.UploadStreamed,
                                streams => new { groupedobjectid = streams.Key, noofstreams = streams.Sum(no => no.Number) }
                            )
                            .SortBy(t => t.noofstreams)
                            .Limit(100)
                            .Lookup(
                               foreignCollection: _uploads,
                               localField: x => x.groupedobjectid,
                               foreignField: (Uploads single) => single.Id,
                               @as: (Uploadstream singles) => singles.uploads
                            )
                            .ToList();
            Console.WriteLine(singlestreams);
            foreach (var i in singlestreams)
            {
                Console.WriteLine(i.ToJson());
            }
            return singlestreams;


        }


        //Top 100 Singles  : TENDING in REGION
        public List<Uploadstream> fetchtop100singlesregiontimestampfilter(string region, int timestamp)
        {
            var singlestreams = _singlestreams.Aggregate()
                            //                         .Match(region => region.Region.Equals(region) ) 
                            //                        .Match(time => int.Parse(time.Timestamp) > timestamp)
                            .Group(
                                key => key.UploadStreamed,
                                streams => new { groupedobjectid = streams.Key, noofstreams = streams.Sum(no => no.Number) }
                            )
                            .SortBy(t => t.noofstreams)
                            .Limit(100)
                            .Lookup(
                               foreignCollection: _uploads,
                               localField: x => x.groupedobjectid,
                               foreignField: (Uploads single) => single.Id,
                               @as: (Uploadstream singles) => singles.uploads
                            )
                            .ToList();
            Console.WriteLine(singlestreams);
            foreach (var i in singlestreams)
            {
                Console.WriteLine(i.ToJson());
            }
            return singlestreams;


        }



        //top 100 playlists 
        public List<CuratedObjectstream> fetchtop100playlists()
        {
            var playlistsstreams = _playliststreams.Aggregate()
                            .Group(
                                key => key.PlaylistStreamed,
                                streams => new { groupedobjectid = streams.Key, noofstreams = streams.Sum(no => no.Number) }
                            )
                            .SortBy(t => t.noofstreams)
                            .Limit(100)
                            .Lookup(
                                foreignCollection: _playlistcurations,
                                localField: x => x.groupedobjectid,
                                foreignField: (Curatedobject playlist) => playlist.Id,
                                @as: (CuratedObjectstream playlist) => playlist.curationobj
                            )
                            .ToList();
            Console.WriteLine(playlistsstreams);
            foreach (var i in playlistsstreams)
            {
                Console.WriteLine(i.ToJson());
            }
            return playlistsstreams;
        }


        //top 100 playlists : REGION
        public List<CuratedObjectstream> fetchtop100playlistsregionfilter(string region)
        {
            var playlistsstreams = _playliststreams.Aggregate()
                            .Match(regions => regions.Region == region)
                            .Group(
                                key => key.PlaylistStreamed,
                                streams => new { groupedobjectid = streams.Key, noofstreams = streams.Sum(no => no.Number) }
                            )
                            .SortBy(t => t.noofstreams)
                            .Limit(100)
                            .Lookup(
                                foreignCollection: _playlistcurations,
                                localField: x => x.groupedobjectid,
                                foreignField: (Curatedobject playlist) => playlist.Id,
                                @as: (CuratedObjectstream playlist) => playlist.curationobj
                            )
                            .ToList();
            Console.WriteLine(playlistsstreams);
            foreach (var i in playlistsstreams)
            {
                Console.WriteLine(i.ToJson());
            }
            return playlistsstreams;
        }


        //top 100 playlists : TRENDING
        public List<CuratedObjectstream> fetchtop100playliststimestampfilter(int timestamp)
        {
            var playlistsstreams = _playliststreams.Aggregate()
                            .Match(time => time.Timestamp > timestamp)
                            .Group(
                                key => key.PlaylistStreamed,
                                streams => new { groupedobjectid = streams.Key, noofstreams = streams.Sum(no => no.Number) }
                            )
                            .SortBy(t => t.noofstreams)
                            .Limit(100)
                            .Lookup(
                                foreignCollection: _playlistcurations,
                                localField: x => x.groupedobjectid,
                                foreignField: (Curatedobject playlist) => playlist.Id,
                                @as: (CuratedObjectstream playlist) => playlist.curationobj
                            )
                            .ToList();
            Console.WriteLine(playlistsstreams);
            foreach (var i in playlistsstreams)
            {
                Console.WriteLine(i.ToJson());
            }
            return playlistsstreams;
        }


        //top 100 playlists : TRENDING in REGION
        public List<CuratedObjectstream> fetchtop100playlistsregiontimestampfilter(int timestamp, string region)
        {
            var playlistsstreams = _playliststreams.Aggregate()
                            .Match(time => time.Timestamp > timestamp)
                            .Match(regions => regions.Region == region)
                            .Group(
                                key => key.PlaylistStreamed,
                                streams => new { groupedobjectid = streams.Key, noofstreams = streams.Sum(no => no.Number) }
                            )
                            .SortBy(t => t.noofstreams)
                            .Limit(100)
                            .Lookup(
                                foreignCollection: _playlistcurations,
                                localField: x => x.groupedobjectid,
                                foreignField: (Curatedobject playlist) => playlist.Id,
                                @as: (CuratedObjectstream playlist) => playlist.curationobj
                            )
                            .ToList();
            Console.WriteLine(playlistsstreams);
            foreach (var i in playlistsstreams)
            {
                Console.WriteLine(i.ToJson());
            }
            return playlistsstreams;
        }





        //top 100 albumns
        public List<CuratedObjectstream> fetchtop100albumns()
        {
            var albumnsstreams = _albumnstreams.Aggregate()
                            .Group(
                                key => key.AlbumnStreamed,
                                streams => new { groupedobjectid = streams.Key, noofstreams = streams.Sum(no => no.Number) }
                            )
                            .SortBy(t => t.noofstreams)
                            .Limit(100)
                            .Lookup(
                                foreignCollection: _albumcurations,
                                localField: x => x.groupedobjectid,
                                foreignField: (Curatedobject albumn) => albumn.Id,
                                @as: (CuratedObjectstream albumn) => albumn.curationobj
                            )
                            .ToList();
            Console.WriteLine(albumnsstreams);
            foreach (var i in albumnsstreams)
            {
                Console.WriteLine(i.ToJson());
            }
            return albumnsstreams;
        }


        //top 100 albumns : REGION
        public List<CuratedObjectstream> fetchtop100albumnsregionfilter(string region)
        {
            var albumnsstreams = _albumnstreams.Aggregate()
                            .Match(regions => regions.Region == region)
                            .Group(
                                key => key.AlbumnStreamed,
                                streams => new { groupedobjectid = streams.Key, noofstreams = streams.Sum(no => no.Number) }
                            )
                            .SortBy(t => t.noofstreams)
                            .Limit(100)
                            .Lookup(
                                foreignCollection: _albumcurations,
                                localField: x => x.groupedobjectid,
                                foreignField: (Curatedobject albumn) => albumn.Id,
                                @as: (CuratedObjectstream albumn) => albumn.curationobj
                            )
                            .ToList();
            Console.WriteLine(albumnsstreams);
            foreach (var i in albumnsstreams)
            {
                Console.WriteLine(i.ToJson());
            }
            return albumnsstreams;
        }


        //top 100 albumns : TRENDING
        public List<CuratedObjectstream> fetchtop100albumnstimestampfilter(int timestamp)
        {
            var albumnsstreams = _albumnstreams.Aggregate()
                            .Match(time => time.Timestamp > timestamp)
                            .Group(
                                key => key.AlbumnStreamed,
                                streams => new { groupedobjectid = streams.Key, noofstreams = streams.Sum(no => no.Number) }
                            )
                            .SortBy(t => t.noofstreams)
                            .Limit(100)
                            .Lookup(
                                foreignCollection: _albumcurations,
                                localField: x => x.groupedobjectid,
                                foreignField: (Curatedobject albumn) => albumn.Id,
                                @as: (CuratedObjectstream albumn) => albumn.curationobj
                            )
                            .ToList();
            Console.WriteLine(albumnsstreams);
            foreach (var i in albumnsstreams)
            {
                Console.WriteLine(i.ToJson());
            }
            return albumnsstreams;
        }


        //top 100 albumns : TRENDING in REGION
        public List<CuratedObjectstream> fetchtop100albumnsregiontimestampfilter(int timestamp, string region)
        {
            var albumnsstreams = _albumnstreams.Aggregate()
                            .Match(time => time.Timestamp > timestamp)
                            .Match(regions => regions.Region == region)
                            .Group(
                                key => key.AlbumnStreamed,
                                streams => new { groupedobjectid = streams.Key, noofstreams = streams.Sum(no => no.Number) }
                            )
                            .SortBy(t => t.noofstreams)
                            .Limit(100)
                            .Lookup(
                                foreignCollection: _albumcurations,
                                localField: x => x.groupedobjectid,
                                foreignField: (Curatedobject albumn) => albumn.Id,
                                @as: (CuratedObjectstream albumn) => albumn.curationobj
                            )
                            .ToList();
            Console.WriteLine(albumnsstreams);
            foreach (var i in albumnsstreams)
            {
                Console.WriteLine(i.ToJson());
            }
            return albumnsstreams;
        }



        //fetch upload thumnail
        public async Task<FileModel> fetchthumbnails(string uploadid, bool curatedobj, string type)
        {
            Console.WriteLine("IIIIID " + uploadid);
            FileModel thumbnail = new FileModel();
            if (curatedobj == true)
            {
                //curated objects playlists and albumns 
                if (type == "playlist")
                {
                    var playlists = fetchcuratedobject(uploadid, "playlist");
                    var bytes = await readbytes(playlists.Pathtothumbnail);
                    thumbnail.bytes = bytes;
                    thumbnail.mimetype = playlists.Thumbnailmimetype;
                    return thumbnail;
                }
                else if (type == "albumn")
                {
                    var albumn = fetchcuratedobject(uploadid, "albumn");
                    var bytes = await readbytes(albumn.Pathtothumbnail);
                    thumbnail.bytes = bytes;
                    thumbnail.mimetype = albumn.Thumbnailmimetype;
                    return thumbnail;
                }
                else
                {
                    return null;
                }
            }
            else if (curatedobj == false)
            {
                var upload = fetchupload(uploadid);
                var bytes = await readbytes(upload.Thumbnailpath);
                thumbnail.bytes = bytes;
                thumbnail.mimetype = upload.Thumbnailmimetype;
                return thumbnail;
            }
            else
            {
                return null;
            }
        }


        public List<LookUpCuratedobjsaves> fetchsavesforclient(string id, string type)
        {               
            if (type == "album")
            {
                var response = _albumsavecurations.Aggregate()
                              .Match(albumn => albumn.CurationId == id)
                              .Lookup(
                                  foreignCollection: _uploads,
                                  localField: x => x.Upload,
                                  foreignField: (Uploads upload) => upload.Id,
                                  @as: (LookUpCuratedobjsaves upload) => upload.uploads
                              )
                              .ToList();

                return response;
            }
            else if (type == "playlist")
            {
                var response = _playlistsavecurations.Aggregate()
                                            .Match(playlist => playlist.CurationId == id)
                                            .Lookup(
                                                foreignCollection: _uploads,
                                                localField: x => x.Upload,
                                                foreignField: (Uploads upload) => upload.Id,
                                                @as: (LookUpCuratedobjsaves upload) => upload.uploads
                                            )
                                            .ToList();
                Console.WriteLine(response);
                foreach (var i in response)
                {
                    Console.WriteLine(i.ToJson());
                }
                return response;
            }
            else
            {
                return null;
            }
        }

        public async Task<FileModel> streamuploadfile(string uploadid)
        {
            FileModel musicfile = new FileModel();
            var response = _uploads.Find<Uploads>(upload => upload.Id == uploadid).FirstOrDefault();
            if (response == null)
            {
                return null;
            }
            else
            {
                var bytes = await readbytes(response.Musicfilepath);
                musicfile.bytes = bytes;
                musicfile.mimetype = response.Musicfilemimetype;
                return musicfile;
            }
        }

        //reads any files
        public async Task<byte[]> readbytes(string filepath)
        {
            byte[] result;
            //byte[] result = File.ReadAllBytes(filepath);
            using (FileStream SourceStream = File.Open(filepath, FileMode.Open))
            {
                result = new byte[SourceStream.Length];
                await SourceStream.ReadAsync(result, 0, (int)SourceStream.Length);
            }
            return result;
        }


    }
}

//thumbnail and upload model
public class FileModel
{
    public byte[] bytes { get; set; }

    public string mimetype { get; set; }

}





//lookup: album or playlist saves 
public class LookUpCuratedobjsaves
{
    public string Id { get; set; }

    public string upload { get; set; }

    public string curationid { get; set; }

    public int timestamp { get; set; }

    public IEnumerable<Upload> uploads { get; set; }

}












//lookup : top 100 playlists and albumns with and without region
public class CuratedObjectstream
{
    public string Id { get; set; }

    public string groupedobjectid { get; set; }

    public int noofstreams { get; set; }

    public IEnumerable<CuratedObjectcuration> curationobj { get; set; }
}

public class CuratedObjectcuration
{
    public string Id { get; set; }

    public string name { get; set; }

    public string pathtothumbnail { get; set; }

    public string thumbnailmimetype { get; set; }

    public string curator { get; set; }

    public int timestamp { get; set; }

}



//lookup : top 100 singles with and without region
public class Uploadstream
{
    public string Id { get; set; }

    public string groupedobjectid { get; set; }

    public int noofstreams { get; set; }

    public IEnumerable<Upload> uploads { get; set; }
}

public class Upload
{
    public string Id { get; set; }

    public string displayname { get; set; }

    public string uploader { get; set; }

    public bool downloadable { get; set; }

    public int downloads { get; set; }

    public bool isprivate { get; set; }

    public int views { get; set; }

    public bool reported { get; set; }

    public int timestamp { get; set; }

    public string thumbnailpath { get; set; }

    public long thumbnailsize { get; set; }

    public string thumbnailmimetype { get; set; }

    public string thumbnailext { get; set; }

    public string musicfilepath { get; set; }

    public long musicfilesize { get; set; }

    public string musicfilemimetype { get; set; }

    public string musicfileext { get; set; }

    public string artistid { get; set; }

}

