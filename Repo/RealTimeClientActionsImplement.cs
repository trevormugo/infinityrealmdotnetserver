using System.Threading.Tasks;
using System;
using System.Net;
using infinityrealmapi.Model;
using infinityrealmapi.RequestDtos;
using MongoDB.Driver;
using System.Collections.Generic;
using MongoDB.Bson;

namespace infinityrealmapi.Repo
{
    public class RealTimeClientActionsImplement : IRealTimeClientActionsInterface
    {
        private IMongoCollection<Signuppost> _accounts;

        private IMongoCollection<Follows> _follows;

        private IMongoCollection<Notifications> _notifications;

        private IMongoCollection<Likes> _likes;

        private IMongoCollection<Chats> _chats;

        private IMongoCollection<Markers> _markers;

        private IMongoCollection<ChatLikes> _chatlikes;

        private IMongoCollection<ChatReplies> _chatreplies;

        private IMongoCollection<MarkerRSVP> _markerrsvp;

        private IMongoCollection<Uploads> _uploads;

        private readonly static Dictionary<string, HashSet<string>> connectionmappings = new Dictionary<string, HashSet<string>>();

        public RealTimeClientActionsImplement(IMongoClient client)
        {
            var database = client.GetDatabase("infinityrealm");
            _accounts = database.GetCollection<Signuppost>("clientdetails");
            _follows = database.GetCollection<Follows>("follows");
            _notifications = database.GetCollection<Notifications>("notifications");
            _likes = database.GetCollection<Likes>("likes");
            _chats = database.GetCollection<Chats>("chats");
            _markers = database.GetCollection<Markers>("markers");
            _chatlikes = database.GetCollection<ChatLikes>("chatlikes");
            _chatreplies = database.GetCollection<ChatReplies>("chatreplies");
            _markerrsvp = database.GetCollection<MarkerRSVP>("markerrsvp");
            _uploads = database.GetCollection<Uploads>("uploads");

        }

        public Signuppost FetchDoerAccount(string id)
        {
            var response = _accounts.Find<Signuppost>(account => account.Id == id).FirstOrDefault();
            return response;
        }


        public Signuppost FetchRecieverAccount(string id)
        {
            var response = _accounts.Find<Signuppost>(account => account.Id == id).FirstOrDefault();
            return response;
        }


        public Follows FollowAccount(Follows request)
        {
            _follows.InsertOne(request);
            return request;
        }


        public void UnFollowAccount(Follows request)
        {
            _follows.FindOneAndDelete(account => account.Account == request.Account && account.AccountFollowed == request.AccountFollowed);
        }



        public void AddUserToStorage(string userid, string connectionid)
        {
            lock (connectionmappings)
            {
                //did this user already have a connectin mapping 
                HashSet<string> connections;
                if (!connectionmappings.TryGetValue(userid, out connections))
                {
                    //userid is not stored  
                    connections = new HashSet<string>();
                    connectionmappings.Add(userid, connections);
                }
                //user was already stored just map another device to him  
                lock (connections)
                {
                    connections.Add(connectionid);
                }
            }
        }


        public void RemoveUserFromStorage(string userid, string connectionid)
        {
            lock (connectionmappings)
            {
                HashSet<string> connections;
                //id the user even stored here 
                if (!connectionmappings.TryGetValue(userid, out connections))
                {
                    //userid is not stored  
                    return;
                }
                //there was a user found remove id or user becoz he has disconnected   
                lock (connections)
                {
                    connections.Remove(connectionid);
                    if (connections.Count == 0)
                    {
                        //remove user if there are no more connection ids mapped to him   
                        connectionmappings.Remove(userid);
                    }
                }
            }
        }


        public HashSet<string> FetchUserConnectionId(string userid)
        {
            HashSet<string> connectionids;
            lock (connectionmappings)
            {
                HashSet<string> connections;
                if (!connectionmappings.TryGetValue(userid, out connections))
                {
                    //user is disconnected 
                    return null;
                }
                connectionids = connectionmappings[userid];
            }
            return connectionids;
        }

        public void SaveNotificationObject(Notifications notification)
        {
            _notifications.InsertOne(notification);
        }

        public void SendPushNotification()
        {
            throw new NotImplementedException();
        }

        public Follows FetchFollow(Follows request)
        {
            var response = _follows.Find<Follows>(follow => follow.Account == request.Account && follow.AccountFollowed == request.AccountFollowed).FirstOrDefault();
            return response;
        }

        public void RemoveNotificationObject(string toaccount, string fromaccount, string type)
        {
            _notifications.FindOneAndDelete<Notifications>(notification => notification.ToAccount == toaccount && notification.FromAccount == fromaccount && notification.NotificationType == type);
        }

        public void SaveLike(Likes request)
        {
            _likes.InsertOne(request);
        }

        public void RemoveLike(LikeRequest request)
        {
            _likes.FindOneAndDelete<Likes>(like => like.Account == request.Account && like.UploadLiked == request.UploadLiked);
        }

        public Likes FetchLike(LikeRequest request)
        {
            var response = _likes.Find<Likes>(like => like.Account == request.Account && like.UploadLiked == request.UploadLiked).FirstOrDefault();
            return response;
        }

        public void AddMarker(Markers request)
        {
            _markers.InsertOne(request);
        }

        public void RemoveMarker(string id)
        {
            _markers.FindOneAndDelete<Markers>(marker => marker.Id == id);
        }

        public Markers FetchMarker(MarkerRequest request)
        {
            var response = _markers.Find<Markers>(marker => marker.Account == request.Account && marker.Latitude == request.Latitude && marker.Longitude == request.Longitude).FirstOrDefault();
            return response;
        }

        public void AddRSVPToMarker(MarkerRSVP request)
        {
            _markerrsvp.InsertOne(request);
        }

        public MarkerRSVP FetchRSVP(MarkerRSVPRequest request)
        {
            var response = _markerrsvp.Find<MarkerRSVP>(rsvp => rsvp.Account == request.Account && rsvp.Marker == request.Marker).FirstOrDefault();
            return response;
        }

        public void SendChat(Chats request)
        {
            _chats.InsertOne(request);
        }

        public void UnsendChat(string id)
        {
            _chats.FindOneAndDelete(chat => chat.Id == id);
        }

        public void LikeChat(ChatLikes request)
        {
            _chatlikes.InsertOne(request);
        }

        public void UnlikeChat(string id)
        {
            _chatlikes.FindOneAndDelete(chat => chat.Id == id);
        }

        public void ReplyToChat(ChatReplies request)
        {
            _chatreplies.InsertOne(request);
        }

        public void DeleteReply(string id)
        {
            _chatreplies.FindOneAndDelete(chat => chat.Id == id);
        }

        public Chats FetchChat(string id)
        {
            var response = _chats.Find<Chats>(chat => chat.Id == id).FirstOrDefault();
            return response;
        }

        public void ForwardChat(ChatForwardRequest request)
        {
            Chats chat = FetchChat(request.ChatId);
            foreach (var acc in request.ToAccounts)
            {
                _chats.InsertOne(chat);
            }
        }

        public Uploads FetchAccountIdByUploadId(string id)
        {
            var response = _uploads.Find<Uploads>(upload => upload.Uploader == id).FirstOrDefault();
            return response;
        }

        public List<Followers> FetchFollowers(string id)
        {
            var followers = _follows.Aggregate()
                            .Match(account => account.AccountFollowed == id)
                            .Lookup(
                                foreignCollection: _accounts,
                                localField: x => x.Account,
                                foreignField: y => y.Id,
                                @as: (Followers follower) => follower.Accounts
                            )
                            .ToList();
            Console.WriteLine(followers);
            foreach (var i in followers)
            {
                Console.WriteLine(i.ToJson());
            }
            return followers;
        }

        public Markers FetchMarkerById(string id)
        {
            var response = _markers.Find<Markers>(marker => marker.Id == id).FirstOrDefault();
            return response;
        }

        public void RemoveRSVPFromMarker(string markerid, string accountid)
        {
            _markerrsvp.FindOneAndDelete(rsvp => rsvp.Marker == markerid && rsvp.Account == accountid);
        }

       
    }
}

//lookup : followers
public class Followers
{
    public string account { get; set; }

    public string accountfollowed { get; set; }

    public int timestamp { get; set; }

    public List<FollowerAccounts> Accounts { get; set; }
}

public class FollowerAccounts
{
    public string Id { get; set; }

    public string name { get; set; }

    public string user { get; set; }

    public string email { get; set; }

    public string phonenumber { get; set; }

    public string thumbnailpath { get; set; }

    public long thumbnailsize { get; set; }

    public string thumbnailmimetype { get; set; }

    public string thumbnailext { get; set; }

    public string password { get; set; }

    public int timestamp { get; set; }

}

