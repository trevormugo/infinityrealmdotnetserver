using infinityrealmapi.Model;
using infinityrealmapi.RequestDtos;
using infinityrealmapi.ResponseDtos;

namespace infinityrealmapi.Repo
{
    public interface IUseraccountsinterface
    {
        Signuppost signup(Signuppost data);

        Signuppost fetchaccount(string id);

        Signuppost fetchaccountbyemail(string email);

        Signuppost userlogin(Loginrequest credentials);

        bool passwordreset(Signuppost resetbody);

    }
}