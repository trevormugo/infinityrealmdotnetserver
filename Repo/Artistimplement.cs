using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using infinityrealmapi.Model;
using infinityrealmapi.RequestDtos;
using Microsoft.AspNetCore.Http;
using MongoDB.Bson;
using MongoDB.Driver;
namespace infinityrealmapi.Repo
{
    public class ArtistImplement : IArtistinterface
    {
        //this class is only about the collection below
        private IMongoCollection<Markers> _markers;

        private IMongoCollection<Signuppost> _details;

        private IMongoCollection<Follows> _follows;
        private IMongoCollection<Uploads> _uploads;
        private IMongoCollection<Artists> _artists;
        private IMongoCollection<Streams> _singlestreams;

        public ArtistImplement(IMongoClient client)
        {
            var database = client.GetDatabase("infinityrealm");
            _markers = database.GetCollection<Markers>("markers");
            _details = database.GetCollection<Signuppost>("clientdetails");
            _follows = database.GetCollection<Follows>("follows");
            _uploads = database.GetCollection<Uploads>("uploads");
            _artists = database.GetCollection<Artists>("artists");
            _singlestreams = database.GetCollection<Streams>("singlestreams");
        }

        public List<Artists> fetchfollowedartists(string id)
        {
            return null;
        }

        public List<Uploads> fetchuploadbyartistid(string id)
        {
            var response = _uploads.Find<Uploads>(upload => upload.ArtistId == id).ToList();
            return response;
        }
    }
}
//lookup : top 100 singles with and without region
public class UploadstreamforArtist
{
    public ObjectId Id { get; set; }

    public string groupedobjectid { get; set; }

    public int noofstreams { get; set; }

    public IEnumerable<Upload> uploads { get; set; }
}