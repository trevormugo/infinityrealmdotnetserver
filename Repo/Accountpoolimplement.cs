using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using infinityrealmapi.RequestDtos;
using infinityrealmapi.ResponseDtos;
using Square;
using Square.Apis;
using Square.Exceptions;
using Square.Models;

namespace infinityrealmapi.Repo
{
    public class Accountpoolimplement : IAccountpoolinterface
    {  
        SquareClient client = new SquareClient.Builder()   
               .Environment(Square.Environment.Sandbox)  
               .AccessToken("EAAAELza192xseTTWQN8bepOrtjEOeKi1cQTguVqlgF8wVSq2lbpkNY09V8vRbGW")
               .Build();
             
        ICustomersApi customersApi;
             
        ISubscriptionsApi subscriptionsApi;
             
        ICatalogApi catalogApi;     

        public Accountpoolimplement()
        {
          customersApi = client.CustomersApi; 
          subscriptionsApi = client.SubscriptionsApi;  
          catalogApi = client.CatalogApi;  
        }
        public async Task<CreateCustomerResponse> createcustomer(Createcustomerrequest body)
        {
            /*var bodyAddress = new Address.Builder()
                .AddressLine1(body.AddressLine1)
                .AddressLine2(body.AddressLine2)
                .AddressLine3(body.AddressLine3)
                .Locality(body.Locality)
                .Sublocality(body.Sublocality)
                .AdministrativeDistrictLevel1(body.AdministrativeDistrictLevel1)
                .PostalCode(body.PostalCode)
                .Country(body.Country)
                .Build();*/
            var customerbody = new CreateCustomerRequest.Builder()
                .IdempotencyKey(Guid.NewGuid().ToString())
                .GivenName(body.Firstnames)
                .FamilyName(body.Surname)
                .CompanyName("company_name2")
                .Nickname(body.Username)
                .EmailAddress(body.Email)
                //.Address(bodyAddress)
                .PhoneNumber(body.Phonenumber)
                .ReferenceId(body.Id)
                .Note("a customer")
                .Build();

            try
            {
                CreateCustomerResponse result = await customersApi.CreateCustomerAsync(customerbody);
                return result;    
            }
            catch (ApiException e){
                InternalExceptionHandler(e);Console.WriteLine("ERROR");
                return null;   
            };
        }

        public async Task<RetrieveCustomerResponse> fetchcustomerbyid(string id)
        {
            try
            {
                RetrieveCustomerResponse result = await customersApi.RetrieveCustomerAsync(id); 
                return result; 
            }
            catch (ApiException e)
            {
                InternalExceptionHandler(e);
                return null; 
            };
        }

        public async Task<CreateCustomerCardResponse> createcustomercard(Createcardrequest body)
        {
            /*var bodyBillingAddress = new Address.Builder()
                    .AddressLine1(body.AddressLine1)
                    .AddressLine2(body.AddressLine2)
                    .AddressLine3(body.AddressLine3)
                    .Locality(body.Locality)
                    .Sublocality(body.Sublocality)
                    .AdministrativeDistrictLevel1(body.AdministrativeDistrictLevel1)
                    .PostalCode(body.PostalCode)
                    .Country(body.Country)
                    .Build();*/
            var cardbody = new CreateCustomerCardRequest.Builder(
                        body.Cardnonce)
                    //.BillingAddress(bodyBillingAddress)
                    .CardholderName(body.FullName)
                    //.VerificationToken(body.VerificationToken)
                    .Build();
            try
            {
                CreateCustomerCardResponse result = await customersApi.CreateCustomerCardAsync(body.CustomerId, cardbody);
                return result;  
            }
            catch (ApiException e)
            {
                InternalExceptionHandler(e);
                return null; 
            };
        }
 
 
 
        public async Task<SearchCustomersResponse> retrievecustomercard(string refid , string phonenumber , string email) 
        {
                
            //we need the customer id first to get to the subscription  
            var bodyQueryFilterReferenceId = new CustomerTextFilter.Builder()
                    .Exact(refid)
                    .Build();
            var bodyQueryFilterEmailAddress = new CustomerTextFilter.Builder()
                    .Exact(email)
                    .Build();
            var bodyQueryFilter = new CustomerFilter.Builder()
                    .EmailAddress(bodyQueryFilterEmailAddress)
                    .ReferenceId(bodyQueryFilterReferenceId) 
                    .Build();
            var bodyQuerySort = new CustomerSort.Builder()
                    .Field("CREATED_AT")
                    .Order("ASC")
                    .Build();
            var bodyQuery = new CustomerQuery.Builder()
                    .Filter(bodyQueryFilter)
                    .Sort(bodyQuerySort)
                    .Build();
            var body = new SearchCustomersRequest.Builder()
                    .Query(bodyQuery)
                    .Build();
            try
            {
                SearchCustomersResponse result = await customersApi.SearchCustomersAsync(body);
                Console.WriteLine(result.Customers[0].Cards.Count);
                if(result.Customers[0].Cards.Count > 0) 
                {
                    return result; 
                }
                else
                {
                    return null;
                }
            }
            catch (ApiException e)
            {
                InternalExceptionHandler(e);
                return null;
            };
        } 
 
 
        //from here we use square model as a request here   
        public async Task<CreateSubscriptionResponse> createsubscription(CreateSubscriptionRequestInternal subscriptionbody)
        { 
            var body = new CreateSubscriptionRequest.Builder(
                    Guid.NewGuid().ToString(),
                    subscriptionbody.LocationId,
                    subscriptionbody.PlanId,
                    subscriptionbody.CustomerId)
                .CardId(subscriptionbody.CardId)
                .Build();

            try
            {
                CreateSubscriptionResponse result = await subscriptionsApi.CreateSubscriptionAsync(body);
                return result; 
            }
            catch (ApiException e){
                InternalExceptionHandler(e);
                return null; 
            };
        }    
 
        public async Task<CancelSubscriptionResponse> cancelsubscription(string subscriptionId)  
        {    
            try
            {
                CancelSubscriptionResponse result = await subscriptionsApi.CancelSubscriptionAsync(subscriptionId);
                return result;   
            }
            catch (ApiException e){
                InternalExceptionHandler(e);
                return null;
            };
        }
 
          
        public async Task<RetrieveSubscriptionResponse> retrievesubscription(string subscriptionid)  
        {
            try
            {
                RetrieveSubscriptionResponse result = await subscriptionsApi.RetrieveSubscriptionAsync(subscriptionid);
                return result;  
            }
            catch (ApiException e)
            {
                InternalExceptionHandler(e);
                return null;
            };
        } 

        public async Task<SearchSubscriptionsResponse> retrievesubscriptionbyrefid(string customerid) 
        {
            var bodyQueryFilterCustomerIds = new List<string>();
            bodyQueryFilterCustomerIds.Add("CHFGVKYY8RSV93M5KCYTG4PN0G");
            var bodyQueryFilter = new SearchSubscriptionsFilter.Builder()
                .CustomerIds(bodyQueryFilterCustomerIds)
                .Build();
            var bodyQuery = new SearchSubscriptionsQuery.Builder()
                .Filter(bodyQueryFilter)
                .Build();
            var body = new SearchSubscriptionsRequest.Builder()
                .Query(bodyQuery)
                .Build();
            try
            {
                SearchSubscriptionsResponse result = await subscriptionsApi.SearchSubscriptionsAsync(body);
                return result; 
            }
            catch (ApiException e)
            {
                InternalExceptionHandler(e);
                return null;
            };
        }

        public async Task<SearchCustomersResponse> fetchcustomerbyrefid(string refid, string email)
        {
            var bodyQueryFilterReferenceId = new CustomerTextFilter.Builder()
                    .Exact("exact0")
                    .Fuzzy(refid)
                    .Build();
            var bodyQueryFilterEmailAddress = new CustomerTextFilter.Builder()
                    .Exact("exact0")
                    .Fuzzy(email)
                    .Build();
            //we need the customer id first to get to the subscription  
            var bodyQueryFilter = new CustomerFilter.Builder()
                    .EmailAddress(bodyQueryFilterEmailAddress)
                    .ReferenceId(bodyQueryFilterReferenceId) 
                    .Build();
            var bodyQuerySort = new CustomerSort.Builder()
                    .Field("CREATED_AT")
                    .Order("ASC")
                    .Build();
            var bodyQuery = new CustomerQuery.Builder()
                    .Filter(bodyQueryFilter)
                    .Sort(bodyQuerySort)
                    .Build();
            var body = new SearchCustomersRequest.Builder()
                    .Cursor("cursor0")
                    .Limit(2L)
                    .Query(bodyQuery)
                    .Build();
            try
            {
                SearchCustomersResponse result = await customersApi.SearchCustomersAsync(body);
                return result; 
            }
            catch (ApiException e)
            {
                InternalExceptionHandler(e);
                return null;
            }; 
        }




        //the master app is the client from here    
        public async Task<UpsertCatalogObjectResponse> createsubscriptionplan(Createsubscriptionplanrequest planbody)
        {
            var phases = new List<SubscriptionPhase>();
            var money = new Money.Builder()  
                .Amount(planbody.Amount)  
                .Currency("USD")  
                .Build();
            var subscriptionPhases = new SubscriptionPhase.Builder(
                            "MONTHLY",
                        money)
                    .Build();
            phases.Add(subscriptionPhases);
            var subscriptionPlan = new CatalogSubscriptionPlan.Builder()
                    .Name(planbody.CountryPlan)  
                    .Phases(phases)  
                    .Build();
            var bodyMObject = new CatalogObject.Builder(
                        "SUBSCRIPTION_PLAN",
                        "#plan")
                    .SubscriptionPlanData(subscriptionPlan)
                    .Build();
            var body = new UpsertCatalogObjectRequest.Builder(
                            Guid.NewGuid().ToString(),
                        bodyMObject)
                    .Build();
            try
            {
                UpsertCatalogObjectResponse result = await catalogApi.UpsertCatalogObjectAsync(body);
                return result; 
            }
            catch (ApiException e)
            {
                InternalExceptionHandler(e);
                return null;
            };
        } 

        //the object id is the plan id  
        public async Task<RetrieveCatalogObjectResponse> retrievespecificsubscriptionplan(string objectid)
        {
            try
            {
                RetrieveCatalogObjectResponse result = await catalogApi.RetrieveCatalogObjectAsync(objectid);
                return result; 
            }
            catch (ApiException e)
            {
                InternalExceptionHandler(e);
                return null;
            };      
        }

        public async Task<ListCatalogResponse> listsubscriptionplans()
        {
            string types = "SUBSCRIPTION_PLAN";
            try
            {
                ListCatalogResponse result = await catalogApi.ListCatalogAsync(types);
                return result; 
            }
            catch (ApiException e)
            {
                InternalExceptionHandler(e);
                return null;
            };
        }

        //the object id is the plan id  
        public async Task<DeleteCatalogObjectResponse> deletesubscriptionplan(string objectid)
        {
            try
            {
                DeleteCatalogObjectResponse result = await catalogApi.DeleteCatalogObjectAsync(objectid);
                return result; 
            }
            catch (ApiException e)
            {
                InternalExceptionHandler(e);
                return null;
            };
        }
 
 
        //we can only change the plan name and phase price the other
        //fields have to be the same esp version number   
        public async Task<UpsertCatalogObjectResponse> updatesubscriptionplan(Updatesubscriptionplanrequest planbody)
        {
            var phases = new List<SubscriptionPhase>();
            var money = new Money.Builder()  
                .Amount(planbody.Amount)  
                .Currency("USD")  
                .Build();
            var subscriptionPhases = new SubscriptionPhase.Builder(
                            "MONTHLY",
                        money)
                    .Build();
            phases.Add(subscriptionPhases);
            var subscriptionPlan = new CatalogSubscriptionPlan.Builder()
                    .Name(planbody.CountryPlan)  
                    .Phases(phases)  
                    .Build();
            var bodyMObject = new CatalogObject.Builder(
                        "SUBSCRIPTION_PLAN",
                        "#plan")
                    .SubscriptionPlanData(subscriptionPlan)
                    .Version(planbody.versionnumber) 
                    .Build();
            var body = new UpsertCatalogObjectRequest.Builder(
                            Guid.NewGuid().ToString(),
                        bodyMObject)
                    .Build();
            try
            {
                UpsertCatalogObjectResponse result = await catalogApi.UpsertCatalogObjectAsync(body);
                return result; 
            }
            catch (ApiException e)
            {
                InternalExceptionHandler(e);
                return null;
            };
        }
         
        public async Task<SearchCatalogObjectsResponse> searchspecificsubscriptionplan(string planname)
        {
            var bodyObjectTypes = new List<string>();
            bodyObjectTypes.Add("SUBSCRIPTION_PLAN");
         
            var bodyQueryExactQuery = new CatalogQueryExact.Builder(
                "name",
                planname)
                .Build();
         
            var bodyQuery = new CatalogQuery.Builder()
                .ExactQuery(bodyQueryExactQuery)
                .Build();
            var body = new SearchCatalogObjectsRequest.Builder()
                .ObjectTypes(bodyObjectTypes)
                .IncludeDeletedObjects(false)
                .IncludeRelatedObjects(false)
                .Query(bodyQuery)
                .Build();
            try
            {
                SearchCatalogObjectsResponse result = await catalogApi.SearchCatalogObjectsAsync(body);
                return result; 
            }
            catch (ApiException e)
            {
                InternalExceptionHandler(e);Console.WriteLine("ERROR");
                return null;
            };
        }
         
        public void InternalExceptionHandler(ApiException e)
        {
            var errors = e.Errors;
            var statusCode = e.ResponseCode;
            var httpContext = e.HttpContext;
            Console.WriteLine("ApiException occurred:");
            Console.WriteLine("Headers:");
            foreach (var item in httpContext.Request.Headers)
            {
                //Display all the headers except Authorization
                if (item.Key != "Authorization")
                {
                    Console.WriteLine("\t{0}: \t{1}", item.Key, item.Value);
                }
            }
            Console.WriteLine("Status Code: \t{0}", statusCode);
            foreach (Error error in errors)
            {
                Console.WriteLine("Error Category:{0} Code:{1} Detail:{2}", error.Category, error.Code, error.Detail);
            }
        }

    }
}