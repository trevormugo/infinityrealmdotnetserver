using System;
using System.Net;
using System.Net.Mail;
using infinityrealmapi.Model;
using infinityrealmapi.RequestDtos;
using MongoDB.Driver;

//we use a new class because we are dealing with a different collection 
namespace infinityrealmapi.Repo
{
    public class Verificationimplement : IVerificationinterface
    {
        private IMongoCollection<VerifiedAccounts> _verifiedaccounts; 
        private SmtpClient _client; 
        private MailMessage _mailmessage;
        private MailAddress _mailaddress;

        public Verificationimplement(IMongoClient client )
        {
          var database = client.GetDatabase("infinityrealm");
          _verifiedaccounts = database.GetCollection<VerifiedAccounts>("verifiedaccounts");    
          _client = new SmtpClient();
          _client.Port = 587;//or use 587
          _client.Host = "smtp.gmail.com";
          _client.EnableSsl = true;
          _client.UseDefaultCredentials = false;
          _client.Credentials = new NetworkCredential("trevormugolawrence@gmail.com", "aquinas123");  
          _mailmessage = new MailMessage();
          _mailaddress = new MailAddress("trevormugolawrence@gmail.com"); 
          _mailmessage.From = _mailaddress;  

        }

        public VerifiedAccounts verifycode(string email , int code)
        {            
            var response = _verifiedaccounts.Find<VerifiedAccounts>(s => (email == s.Email) && (code == s.Codesent)).FirstOrDefault();
            return response;
        }

        public VerifiedAccounts fetchbyid(string id)
        {
            var response = _verifiedaccounts.Find<VerifiedAccounts>(s => id == s.Id).FirstOrDefault();
            return response;  
        }

        public VerifiedAccounts fetchbyemail(string email)
        {
            var response = _verifiedaccounts.Find<VerifiedAccounts>(s => email == s.Email).FirstOrDefault();
            return response; 
        }

      
        public bool sendcode(VerifiedAccounts emailobj , bool insertobj)
        {
            var rand = new Random();
            var sent = rand.Next(1000,9999);
            System.Exception code = null;
            emailobj.Codesent = sent; 
            Console.WriteLine(sent);
            _mailmessage.To.Add(emailobj.Email);
            _mailmessage.Body = "The code is:" + sent;
            _mailmessage.Subject = "Verification";
            try
            {
                _client.Send(_mailmessage);
            }
            catch (SmtpException smtpNotFound)
            {
                code = smtpNotFound.InnerException;
            }
            if(code == null)
            { 
                if(insertobj == true) 
                {
                    _verifiedaccounts.InsertOne(emailobj);
                }
                else
                {
                    _verifiedaccounts.ReplaceOne<VerifiedAccounts>(model => model.Email == emailobj.Email , emailobj);
                }
                return true;
            }
            else
            {
              return false;
            } 
        }

        public void deleterequest(string email)
        {
          _verifiedaccounts.DeleteOne<VerifiedAccounts>(s => s.Email == email);
        }
    }
}