using System.Collections.Generic;
using System.Threading.Tasks;
using infinityrealmapi.Model;
using infinityrealmapi.RequestDtos;
using infinityrealmapi.ResponseDtos;
using Microsoft.AspNetCore.Http;
using MongoDB.Bson;

namespace infinityrealmapi.Repo
{
    public interface IUploadsinterface
    {
        Uploads savetodb(Uploads data, IFormFile thumbnail, IFormFile musicfile, string thubnailext, string musicfileext, string thumbnailpath, string musicpath);

        Curatedobject storeobjindb(Curatedobject data, IFormFile thumbnail, string thumbnailpath, string type);

        Curatedobjsaves saveincuration(string id, Curatedobjsaves data, string type);

        Curatedobjsaves fetchcurationsave(string id, string type);

        Uploads fetchupload(string id);

        Curatedobject fetchcuratedobject(string id, string type);

        PlaylistStreams insertplayliststream(PlaylistStreams request);

        PlaylistStreams updateplayliststream(PlaylistStreamRequest request, PlaylistStreams streamobj);

        PlaylistStreams fetchplayliststreambyid(string id);

        PlaylistStreams fetchplayliststreambycredentials(PlaylistStreamRequest request);

        AlbumnStreams insertalbumnstream(AlbumnStreams request);

        AlbumnStreams updatealbumnstream(AlbumnStreamRequest request, AlbumnStreams streamobj);

        AlbumnStreams fetchalbumnstreambycredentials(AlbumnStreamRequest request);

        AlbumnStreams fetchalbumnstreambyid(string id);

        Streams insertsinglestream(Streams request);

        Streams updatesinglestream(StreamRequest request, Streams streamobj);

        Streams fetchsinglestreambycredentials(StreamRequest request);

        Streams fetchsinglestreambyid(string id);

        List<CuratedObjectstream> fetchtop100playlists();

        List<CuratedObjectstream> fetchtop100playlistsregionfilter(string region);

        List<CuratedObjectstream> fetchtop100playliststimestampfilter(int timestamp);

        List<CuratedObjectstream> fetchtop100playlistsregiontimestampfilter(int timestamp, string region);

        List<Uploadstream> fetchtop100singles();

        List<Uploadstream> fetchtop100singlesregionfilter(string region);

        List<Uploadstream> fetchtop100singlestimestampfilter(int timestamp);

        List<Uploadstream> fetchtop100singlesregiontimestampfilter(string region, int timestamp);

        List<CuratedObjectstream> fetchtop100albumns();

        List<CuratedObjectstream> fetchtop100albumnsregionfilter(string region);

        List<CuratedObjectstream> fetchtop100albumnstimestampfilter(int timestamp);

        List<CuratedObjectstream> fetchtop100albumnsregiontimestampfilter(int timestamp, string region);


        Task<FileModel> fetchthumbnails(string uploadid, bool curatedobj, string type);


        List<LookUpCuratedobjsaves> fetchsavesforclient(string id, string type);

        Task<FileModel> streamuploadfile(string uploadid);

           

    }
}