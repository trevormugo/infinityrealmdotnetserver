using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace infinityrealmapi.Model
{
    public class Notifications
    {

        [BsonId(IdGenerator = typeof(BlogIdGenerator))]
        public string Id { get; set; }

        [BsonElement("toaccount")]
        public string ToAccount { get; set; }

        [BsonElement("fromaccount")]
        public string FromAccount { get; set; }

        [BsonElement("fromaccountname")]
        public string FromAccountName { get; set; }

        [BsonElement("fromaccountthumbnail")]
        public string FromAccountThumbnail { get; set; }

        [BsonElement("notificationtype")]
        public string NotificationType { get; set; }

        [BsonElement("notificationstring")]
        public string NotificationString { get; set; }

        [BsonElement("timestamp")]
        public int Timestamp { get; set; } = Convert.ToInt32((int)DateTime.Now.Subtract(new DateTime(1970, 1, 1)).TotalSeconds);

    }
}