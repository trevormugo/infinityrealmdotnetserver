using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace infinityrealmapi.Model
{
    public class MarkerRSVP
    {

        [BsonId(IdGenerator = typeof(BlogIdGenerator))]
        public string Id{get; set; }
 
        [BsonElement("account")]
        public string Account{get; set; }

        [BsonElement("marker")]
        public string Marker{get; set; }

        [BsonElement("timeuserrsvpd")]
        public int Timestamp{get; set;} = Convert.ToInt32((int)DateTime.Now.Subtract(new DateTime(1970, 1, 1)).TotalSeconds);
        

    }
} 