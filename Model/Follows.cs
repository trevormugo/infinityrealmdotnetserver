using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace infinityrealmapi.Model
{
    public class Follows
    {

        [BsonElement("account")]
        public string Account{get; set; }

        [BsonElement("accountfollowed")]
        public string AccountFollowed{get; set; }
  
        [BsonElement("timestamp")]
        public int Timestamp{get; set;} = Convert.ToInt32((int)DateTime.Now.Subtract(new DateTime(1970, 1, 1)).TotalSeconds);
 
    }
} 