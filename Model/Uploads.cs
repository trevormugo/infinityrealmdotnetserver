using System;
using Microsoft.AspNetCore.Http;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;

//Internal model we wont be returning it to the client
namespace infinityrealmapi.Model
{ 
    public class Uploads
    {
        [BsonId(IdGenerator = typeof(BlogIdGenerator))]
        public string Id { get; set; }

        [BsonElement("displayname")]
        public string Displayname { get; set; }

        [BsonElement("uploader")]
        public string Uploader { get; set; }

        [BsonElement("downloadable")]
        public bool Downloadable { get; set; } = false;

        [BsonElement("downloads")]
        public int Downloads { get; set; } = 0;

        [BsonElement("isprivate")]
        public bool IsPrivate { get; set; } = false;

        [BsonElement("views")]
        public int Views { get; set; } = 0;

        [BsonElement("reported")]
        public bool Reported { get; set; } = false;

        [BsonElement("timestamp")]
        public int Timestamp { get; set; } = Convert.ToInt32((int)DateTime.Now.Subtract(new DateTime(1970, 1, 1)).TotalSeconds);

        [BsonElement("thumbnailpath")]
        public string Thumbnailpath { get; set; }

        [BsonElement("thumbnailsize")]
        public long Thumbnailsize { get; set; }

        [BsonElement("thumbnailmimetype")]
        public string Thumbnailmimetype { get; set; }

        [BsonElement("thumbnailext")]
        public string Thumbnailext { get; set; }

        [BsonElement("musicfilepath")]
        public string Musicfilepath { get; set; }

        [BsonElement("musicfilesize")]
        public long Musicfilesize { get; set; }

        [BsonElement("musicfilemimetype")]
        public string Musicfilemimetype { get; set; }

        [BsonElement("musicfileext")]
        public string Musicfileext { get; set; }

        [BsonElement("artistid")]
        public string ArtistId { get; set; }

    }
}