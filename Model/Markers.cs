using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace infinityrealmapi.Model
{
    public class Markers
    {

        [BsonId(IdGenerator = typeof(BlogIdGenerator))]
        public string Id { get; set; }

        [BsonElement("account")]
        public string Account { get; set; }

        [BsonElement("longitude")]
        public double Longitude { get; set; }

        [BsonElement("latitude")]
        public double Latitude { get; set; }

        [BsonElement("type")]
        public string Type { get; set; }

        [BsonElement("thumbnailisset")]
        public bool Thumbnialisset { get; set; }

        [BsonElement("canrsvp")]
        public bool CanRsvp { get; set; }

        [BsonElement("thumbnailpath")]
        public string Thumbnailpath { get; set; }

        [BsonElement("thumbnailsize")]
        public long Thumbnailsize { get; set; }

        [BsonElement("thumbnailmimetype")]
        public string Thumbnailmimetype { get; set; }

        [BsonElement("thumbnailext")]
        public string Thumbnailext { get; set; }

        [BsonElement("description")]
        public string Description { get; set; }

        [BsonElement("views")]
        public int Views { get; set; } = 0;

        [BsonElement("timeusermarkedapoint")]
        public int Timestamp { get; set; } = Convert.ToInt32((int)DateTime.Now.Subtract(new DateTime(1970, 1, 1)).TotalSeconds);


    }
}