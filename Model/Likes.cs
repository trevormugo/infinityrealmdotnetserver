using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace infinityrealmapi.Model
{
    public class Likes
    {

        [BsonRepresentation(BsonType.ObjectId)]
        public string Account{get; set; }

        [BsonRepresentation(BsonType.ObjectId)]
        public string UploadLiked{get; set; }
  
        [BsonElement("timestamp")]
        public int Timestamp{get; set;} = Convert.ToInt32((int)DateTime.Now.Subtract(new DateTime(1970, 1, 1)).TotalSeconds);
 
    }
} 