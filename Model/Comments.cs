using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace infinityrealmapi.Model
{
    public class Comments
    {

        [BsonId(IdGenerator = typeof(BlogIdGenerator))]
        public string Id{get; set; }
 
        [BsonElement("account")]
        public string Account{get; set; }

        [BsonElement("uploadcommentedon")]
        public string UploadCommentedOn{get; set; }

        [BsonElement("comment")]
        public string Comment{get; set; }

        [BsonElement("timeuserstreamed")]
        public int Timestamp{get; set;} = Convert.ToInt32((int)DateTime.Now.Subtract(new DateTime(1970, 1, 1)).TotalSeconds);
 
    }
} 