using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace infinityrealmapi.Model
{
    public class ChatLikes
    {

        [BsonId(IdGenerator = typeof(BlogIdGenerator))]
        public string Id { get; set; }

        [BsonElement("account")]
        public string Account { get; set; }

        [BsonElement("chatliked")]
        public string ChatLiked { get; set; }


    }
}