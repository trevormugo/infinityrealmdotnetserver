using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace infinityrealmapi.Model
{
    public class Podcasts
    {

        [BsonId(IdGenerator = typeof(BlogIdGenerator))]
        public string Id { get; set; }


        [BsonElement("account")]
        public string Account { get; set; }

        [BsonElement("rsslink")]
        public string RssLink { get; set; }

    }
}