using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace infinityrealmapi.Model
{
    public class ChatReplies
    {

        [BsonId(IdGenerator = typeof(BlogIdGenerator))]
        public string Id { get; set; }

        [BsonElement("account")]
        public string Account { get; set; }

        [BsonElement("toaccount")]
        public string ToAccount { get; set; }

        [BsonElement("replyingtochat")]
        public string ReplyingToChat { get; set; }

        [BsonElement("replychatmessage")]
        public string ReplyChatMessage { get; set; }

        [BsonElement("timestamp")]
        public int Timestamp { get; set; } = Convert.ToInt32((int)DateTime.Now.Subtract(new DateTime(1970, 1, 1)).TotalSeconds);

    }
}