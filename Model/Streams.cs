using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace infinityrealmapi.Model
{
    public class Streams
    {

        [BsonId(IdGenerator = typeof(BlogIdGenerator))]
        public string Id{get; set; }

 
        [BsonElement("account")]
        public string Account{get; set; }

        [BsonElement("uploadstreamed")]
        public string UploadStreamed{get; set; }
  
        [BsonElement("timeuserstreamed")]
        public int Timestamp{get; set;} = Convert.ToInt32((int)DateTime.Now.Subtract(new DateTime(1970, 1, 1)).TotalSeconds);
 
        [BsonElement("region")]
        public string Region{get; set;}
 
        [BsonElement("streamno")]
        public int Number{get; set;} = 1;
 

    }
} 