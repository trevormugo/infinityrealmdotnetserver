using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;

namespace infinityrealmapi.Model
{
    public class Curatedobject
    {
        [BsonId(IdGenerator = typeof(BlogIdGenerator))]
        public string Id { get; set; }

        [BsonElement("name")]
        public string Name { get; set; }

        [BsonElement("pathtothumbnail")]
        public string Pathtothumbnail { get; set; }

        [BsonElement("thumbnailmimetype")]
        public string Thumbnailmimetype { get; set; }

        [BsonElement("curator")]
        public string Curator { get; set; }

        [BsonElement("timestamp")]
        public int Timestamp { get; set; } = Convert.ToInt32((int)DateTime.Now.Subtract(new DateTime(1970, 1, 1)).TotalSeconds);

    }
}