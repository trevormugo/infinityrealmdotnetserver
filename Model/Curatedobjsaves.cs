using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace infinityrealmapi.Model
{
    public class Curatedobjsaves
    {
        [BsonId(IdGenerator = typeof(BlogIdGenerator))]
        public string Id{get; set; }

        [BsonElement("upload")]
        public string Upload{get; set; }

        [BsonElement("curationid")]
        public string CurationId{get; set; }
  
        [BsonElement("timestamp")]
        public int Timestamp{get; set;} = Convert.ToInt32((int)DateTime.Now.Subtract(new DateTime(1970, 1, 1)).TotalSeconds);
 
    }
} 