using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace infinityrealmapi.Model
{
    public class VerifiedAccounts
    {
        [BsonId(IdGenerator = typeof(BlogIdGenerator))]
        public string Id{get; set; }

        [BsonElement("email")]
        public string Email{get; set; }

        [BsonElement("dateverified")]
        public string Dateverified{get; set; }

        [BsonElement("codesent")]
        public int Codesent{get; set; }
    }
}