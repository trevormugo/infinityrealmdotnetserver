using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

//Internal model we wont be returning it to the client
namespace infinityrealmapi.Model
{
    public class Artists
    {
        [BsonId(IdGenerator = typeof(BlogIdGenerator))]
        public string Id { get; set; }

        [BsonElement("accountid")]
        public string AccountId { get; set; }

        [BsonElement("fullname")]
        public string Fullname { get; set; }

        [BsonElement("artistname")]
        public string Artistname { get; set; }

        [BsonElement("email")]
        public string Email { get; set; }

        [BsonElement("phonenumber")]
        public string Phonenumber { get; set; }

        [BsonElement("thumbnailpath")]
        public string Thumbnailpath { get; set; }

        [BsonElement("thumbnailsize")]
        public long Thumbnailsize { get; set; }

        [BsonElement("thumbnailmimetype")]
        public string Thumbnailmimetype { get; set; }

        [BsonElement("thumbnailext")]
        public string Thumbnailext { get; set; }

        [BsonElement("password")]
        public string Password { get; set; }

        [BsonElement("timestamp")]
        public int Timestamp { get; set; } = Convert.ToInt32((int)DateTime.Now.Subtract(new DateTime(1970, 1, 1)).TotalSeconds);
    }
}