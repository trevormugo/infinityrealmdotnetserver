using infinityrealmapi.Model;
using Microsoft.AspNetCore.Mvc;
using infinityrealmapi.Repo;
using infinityrealmapi.ResponseDtos;
using AutoMapper;
using infinityrealmapi.RequestDtos;
using System;

namespace infinityrealmapi.Controllers
{
    [Route("api/useraccounts")]
    [ApiController]
    public class UserAccountsController : ControllerBase
    {           
        private IUseraccountsinterface _service; 
        private IMapper _mapper;
        public UserAccountsController(IUseraccountsinterface service , IMapper mapper)
        {
           _service = service;
           _mapper = mapper; 

        }    

        //GET : api/useraccounts/signup/{id}   
        [HttpGet("{id}",Name="FetchAccount")]
        public ActionResult<Signuppostresponse> FetchAccount(string id)
        {
          var response = _service.fetchaccount(id); 
          if(response != null)
          {
            return Ok(response);   
          }
          else
          {
            return NotFound();   
          }
        }  

        //POST : api/useraccounts/login  
        [HttpPost("login")]
        public ActionResult <Signuppost> UserLogin(Loginrequest credentials)
        { 
           var response = _service.userlogin(credentials); 
           if(response == null)
           {
             return BadRequest();
           }
           else
           {
             var sendback = _mapper.Map<Loginresponse>(response);
             return Ok();
           }
        }

        //POST : api/useraccounts/signup   
        [HttpPost("signup")]
        public ActionResult <Signuppostresponse> CreateAccount(Signuppostrequest account)
        {
          //when creating a resource in REST you have to return it 
          //and also return the link to it
          //first check if there is any existing account with that email
          var existingaccounts = _service.fetchaccountbyemail(account.Email); 
          if(existingaccounts != null)
          {
            return BadRequest(account.Email + " already exists in our database");
          }
          else
          {
            //if u ever forget go back to read about DTO'S
            var requestdto = _mapper.Map<Signuppost>(account);
            _service.signup(requestdto);
            var responsedto = _mapper.Map<Signuppostresponse>(requestdto);
            return CreatedAtRoute(nameof(FetchAccount),new {Id = responsedto.Id},responsedto);
          } 
        } 


        //POST : api/useraccounts/resetpassword   
        [HttpPut("resetpassword")]  
        public ActionResult ResetPaswword(PasswordResetRequest resetbody)
        {
            var account = _service.fetchaccountbyemail(resetbody.Email); 
            if(account == null)
            {
                return BadRequest();    
            }
            else
            {
                var reqtomodel = _mapper.Map(resetbody ,account);
                var response = _service.passwordreset(reqtomodel);
                if(response == false)
                {
                   return NoContent(); 
                } 
                else
                {
                   return Ok();  
                }    
            } 
        }   

    }
} 