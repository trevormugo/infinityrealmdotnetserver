using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using infinityrealmapi.Repo;
using infinityrealmapi.ResponseDtos;
using infinityrealmapi.RequestDtos;
using infinityrealmapi.Model;
using System.Collections.Generic;

//we use a new controller because we are dealing with a different resource
namespace infinityrealmapi.Controllers
{
    [Route("api/maps")]
    [ApiController]
    public class MapsController : ControllerBase
    {
        private IMapsinterface _service;
        private IMapper _mapper;
        public MapsController(IMapsinterface service, IMapper mapper)
        {
            _mapper = mapper;
            _service = service;
        }

        //GET : api/maps/fetchfollowingmarkers/id  
        [HttpGet("fetchfollowingmarkers/{id}")]
        public ActionResult<List<MapsFollowing>> FetchFollowingMarkers(string id)
        {
            var response = _service.fetchmarkers(id);
            if (response == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(response);
            }
        }
    }
}