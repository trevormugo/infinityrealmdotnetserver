using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using infinityrealmapi.Repo;
using infinityrealmapi.ResponseDtos;
using infinityrealmapi.RequestDtos;
using infinityrealmapi.Model;
using System.Collections.Generic;

//we use a new controller because we are dealing with a different resource
namespace infinityrealmapi.Controllers
{
    [Route("api/artists")]
    [ApiController]
    public class ArtistController : ControllerBase
    {
        private IArtistinterface _service;
        private IMapper _mapper;
        public ArtistController(IArtistinterface service, IMapper mapper)
        {
            _mapper = mapper;
            _service = service;
        }


        //GET : api/artists/uploads/id  
        [HttpGet("uploads/{id}")]
        public ActionResult FetchUploadByArtistId(string id)
        {
            var response = _service.fetchuploadbyartistid(id);
            if (response == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(response);
            }
        }


        //GET : api/artists/uploads/id  
        [HttpGet("artists/{id}")]
        public ActionResult FetchArtists(string id)
        {
            var response = _service.fetchuploadbyartistid(id);
            if (response == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(response);
            }
        }

    }
}