using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using infinityrealmapi.Repo;
using infinityrealmapi.ResponseDtos;
using infinityrealmapi.RequestDtos;
using infinityrealmapi.Model;

//we use a new controller because we are dealing with a different resource
namespace infinityrealmapi.Controllers
{
    [Route("api/verification")]
    [ApiController]
    public class UserEmailVerificationController : ControllerBase
    {
        private IVerificationinterface _service;
        private IMapper _mapper;
        public UserEmailVerificationController(IVerificationinterface service, IMapper mapper)
        {
            _mapper = mapper;
            _service = service;
        }

        //GET  : api/verification/verifybymodel?email=params&code=params
        [HttpGet("verifybymodel")]
        public ActionResult<bool> VerifyCode([FromQuery] string email, [FromQuery] int code)
        {    
            var result = _service.verifycode(email, code);
            if (result == null)
            {
                return NotFound(result);
            }
            else
            {
                return Ok(result);
            }
        }

        //GET  : api/verification/{id} 
        [HttpGet("{id}", Name = "FetchById")]
        public ActionResult<Sendemailresponse> FetchById(string id)
        {
            var resource = _service.fetchbyid(id);
            if (resource == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(resource);
            }
        }

        //POST : api/verification/signup -creates a resource
        [HttpPost("signup")]
        public ActionResult<Sendemailresponse> SendCode(Sendemailrequest emailreq)
        {
            var resource =  _service.fetchbyemail(emailreq.Email);
            if (resource != null)
            {
                return BadRequest("resource already in repository");
            }
            else
            {
                var emailmodel = _mapper.Map<VerifiedAccounts>(emailreq);
                var response = _service.sendcode(emailmodel, true);
                if (response == false)
                {
                    return NoContent();
                }
                else
                {
                    var emailresponsedto = _mapper.Map<Sendemailresponse>(emailmodel);
                    return CreatedAtRoute(nameof(FetchById), new { Id = emailmodel.Id }, emailresponsedto);
                }
            }
        }


        //PUT : api/verification/resetpassword  - updates a resource
        [HttpPut("resetpassword")]
        public ActionResult<Sendemailresponse> SendCodeForReset(Sendemailrequest emailreq)
        {
            var resource = _service.fetchbyemail(emailreq.Email);
            if (resource == null)
            {
                return BadRequest("Account doesnt exist");
            }
            else
            {
                var emailmodel = _mapper.Map(emailreq, resource);
                var response = _service.sendcode(emailmodel, false);
                if (response == false)
                {
                    return NoContent();
                }
                else
                {
                    return Ok(emailmodel);
                }
            }
        }

        //DELETE : api/verification/deleteresource?email=email  
        [HttpDelete("deleteresource")]
        public ActionResult<Sendemailresponse> DeleteResource([FromQuery] string email)
        {
            var resource = _service.fetchbyemail(email);
            if (resource == null)
            {
                return NotFound("Account doesnt exist");
            }
            else
            {
                _service.deleterequest(email);
                return NoContent();
            }
        }

        
    }
}