using System;
using System.Threading.Tasks;
using AutoMapper;
using infinityrealmapi.Repo;
using infinityrealmapi.RequestDtos;
using Microsoft.AspNetCore.Mvc;
using Square.Models;

namespace infinityrealmapi.Controllers
{

   [Route("api/accountspool")] 
   [ApiController]  
   public class AccountsPoolController : ControllerBase
   {  
        private IAccountpoolinterface _service; 
        private IMapper _mapper;
        public AccountsPoolController(IAccountpoolinterface service , IMapper mapper)
        {
           _service = service;
           _mapper = mapper; 

        }   

        //GET :  api/accountspool/id     
        [HttpGet("{id}",Name="FetchCustomerById")]    
        public async Task<ActionResult<RetrieveCustomerResponse>> FetchCustomerById(string id)
        {
            var response = await _service.fetchcustomerbyid(id); 
            if(response == null)  
            {   
                return NotFound();  
            }     
            else   
            {   
                return Ok(response.Customer);    
            }  
        }     
 
        //POST : api/accountspool/createcustomer  
        [HttpPost("createcustomer")]    
        public async Task<ActionResult<CreateCustomerResponse>> CreateCustomer(Createcustomerrequest body)   
        {
            var response = await _service.createcustomer(body);     
            if(response == null)  
            {   
                return BadRequest();  
            }     
            else   
            {   
                return CreatedAtRoute(nameof(FetchCustomerById),new {Id = response.Customer.Id},response.Customer);
            }  
        }  

        //PUT : api/accountspool/createcustomercard  
        [HttpPut("createcustomercard")]    
        public async Task<ActionResult<CreateCustomerCardResponse>> CreateCustomerCard(Createcardrequest body)   
        {
            var response = await _service.createcustomercard(body);     
            if(response == null)  
            {   
                return BadRequest();  
            }     
            else   
            {   
                return Ok(response.Card);
            }  
        }  

        //GET: api/accountspool/retrievesubscription  
        [HttpGet("retrievesubscription/{id}" , Name="RetrieveSubscription")]  
        public async Task<ActionResult<RetrieveSubscriptionResponse>> RetrieveSubscription(string id) 
        {
            var response = await _service.retrievesubscription(id); 
            if(response == null)  
            {   
                return NotFound();  
            }     
            else   
            {   
                return Ok(response.Subscription);    
            }  
        }  
 

 
        //POST: api/accountspool/createsubscription  
        [HttpPost("createsubscription")] 
        public async Task<ActionResult<CreateSubscriptionResponse>> CreateSubcription(CreateSubscriptionRequestInternal body)  
        {   Console.WriteLine(body.Refid); 
            var customercard = await _service.retrievecustomercard(body.Refid,body.Phonenumber,body.Email);
            if(customercard == null) 
            {
                return NoContent(); 
            }
            else
            {
                var response = await _service.createsubscription(body);     
                if(response == null)  
                {   
                    return BadRequest();  
                }     
                else   
                {   
                    return CreatedAtRoute(nameof(RetrieveSubscription),new {Id = response.Subscription.Id},response.Subscription);
                } 
            }   
            
        } 

        //POST: api/accountspool/cancelsubscription   
        [HttpPost("cancelsubscription")]  
        public async Task<ActionResult<CancelSubscriptionResponse>> CancelSubscription(string id) 
        {
            var response = await _service.cancelsubscription(id);     
            if(response == null)  
            {   
                return BadRequest();  
            }     
            else   
            {   
                return CreatedAtRoute(nameof(RetrieveSubscription),new {Id = response.Subscription.Id},response.Subscription);
            } 
        }   
 
           
        //GET: api/accountspool/retrievesubscriptionbyrefid?refid=refid&email=email   
        [HttpGet("retrievesubscriptionbyrefid")]    
        public async Task<ActionResult<SearchSubscriptionsResponse>> RetrieveSubscriptionByRefId([FromQuery] string refid, [FromQuery] string email)   
        { 
            var customerresponse =  await _service.fetchcustomerbyrefid(refid,email); 
            if(customerresponse == null)   
            {
                return NotFound();   
            } 
            else  
            {
                var response = await _service.retrievesubscriptionbyrefid(customerresponse.Customers[0].Id); 
                if(response == null)  
                {   
                    return NotFound();  
                }      
                else   
                {   
                    return Ok(response.Subscriptions[0]);    
                }  
            }   
        }  

           
        //GET: api/accountspool/fetchcustomerbyrefid?refid=refid&email=email  
        [HttpGet("fetchcustomerbyrefid")]    
        public async Task<ActionResult<SearchCustomersResponse>> FetchCustomerByRefId([FromQuery] string refid, [FromQuery] string email)   
        { 
            var response = await _service.fetchcustomerbyrefid(refid,email); 
            if(response == null)  
            {   
                return NotFound();  
            }     
            else   
            {   
                return Ok(response.Customers[0]);    
            }  
   
        }  
 
        //GET :  api/accountspool/retrievespecificsubscriptionplan/id     
        [HttpGet("retrievespecificsubscriptionplan/{id}" , Name = "RetrieveSpecificSubscriptionPlan")]    
        public async Task <ActionResult<RetrieveCustomerResponse>> RetrieveSpecificSubscriptionPlan(string id)
        {
            var response = await _service.retrievespecificsubscriptionplan(id); 
            if(response == null)  
            {   
                return NotFound();  
            }     
            else   
            {   
                return Ok(response.MObject);    
            }  
        }     
 
        //POST : api/accountspool/createsubscriptionplan  
        [HttpPost("createsubscriptionplan")]    
        public async Task<ActionResult<UpsertCatalogObjectResponse>> CreateSubscriptionPlan(Createsubscriptionplanrequest body)   
        {   
            var searchresponse = await _service.searchspecificsubscriptionplan(body.CountryPlan); 
            if(searchresponse != null) 
            {    
                return NoContent();  
            } 
            else    
            {    
                var response = await _service.createsubscriptionplan(body);     
                if(response == null)  
                {   
                    return BadRequest();  
                }     
                else   
                {   
                    return CreatedAtRoute(nameof(RetrieveSpecificSubscriptionPlan),new {Id = response.CatalogObject.Id},response.CatalogObject);
                }   
            }    
        }

        //DELETE : api/accountspool/deletesubscriptionplan  
        [HttpDelete("deletesubscriptionplan")]    
        public async Task<ActionResult> DeleteSubscriptionPlan(string id)   
        {
            var response = await _service.deletesubscriptionplan(id);     
            if(response == null)  
            {   
                return BadRequest();  
            }     
            else   
            {   
                return NoContent();
            }  
        }

        //PUT : api/accountspool/updatesubscriptionplan  
        [HttpPut("updatesubscriptionplan")]    
        public async Task<ActionResult<UpsertCatalogObjectResponse>> UpdateSubscriptionPlan(Updatesubscriptionplanrequest body)   
        {
            var response = await _service.updatesubscriptionplan(body);     
            if(response == null)  
            {   
                return BadRequest();  
            }     
            else   
            {   
                return NoContent();
            }  
        }

        //GET: api/accountspool/searchspecificsubscriptionplan/id  
        [HttpGet("searchspecificsubscriptionplan/{id}")]  
        public async Task<ActionResult<SearchCatalogObjectsResponse>> SearchSpecificSubscriptionPlan(string id) 
        {               Console.WriteLine("PLAN NAME IS "+id);

            var response = await _service.searchspecificsubscriptionplan(id); 
            if(response == null)  
            {   
                return NotFound();  
            }     
            else   
            {   
                return Ok(response.Objects[0]);    
            }  
        }  


        //GET : api/accountspool/listsubscriptionplans  
        [HttpGet("listsubscriptionplans")]    
        public async Task<ActionResult<ListCatalogResponse>> ListSubscriptionPlans()   
        {
            var response = await _service.listsubscriptionplans();     
            if(response == null)  
            {   
                return NotFound();  
            }     
            else   
            {   
                return Ok(response);
            }  
        }
   }   
} 