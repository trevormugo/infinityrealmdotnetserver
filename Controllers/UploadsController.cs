using System.Threading.Tasks;
using infinityrealmapi.RequestDtos;
using Microsoft.AspNetCore.Mvc;
using infinityrealmapi.Utilities;
using infinityrealmapi.Repo;
using AutoMapper;
using infinityrealmapi.Model;
using System.IO;
using System.Linq;
using System;
using System.Collections.Generic;
using infinityrealmapi.ResponseDtos;

namespace infinityrealmapi.Controllers
{
    [Route("api/uploads")]
    [ApiController]
    public class UploadsController : ControllerBase
    {

        private IUploadsinterface _service;
        private IMapper _mapper;
        private string[] thumbnailExtensions = { ".jpg", ".jpeg", ".png", ".gif" };
        private string[] musicExtensions = { ".wav", ".mp3", ".ogg", ".aac", ".avi", ".flv", ".mp4", ".mov", ".wmv", ".webm", ".webvtt", ".ogv" };

        public UploadsController(IUploadsinterface service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;

        }


        //GET : api/uploads/id   
        [HttpGet("{id}")]
        public ActionResult<Uploads> FecthUpload(string id)
        {
            var response = _service.fetchupload(id);
            if (response == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(response);
            }
        }


        //GET : api/uploads/album/id   
        [HttpGet("album/{id}")]
        public ActionResult<Uploads> FecthCuratedAlbumObject(string id)
        {
            var response = _service.fetchcuratedobject(id, "album");
            if (response == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(response);
            }
        }

        //GET : api/uploads/playlist/id   
        [HttpGet("playlist/{id}", Name = "FecthCuratedPlaylistObject")]
        public ActionResult<Uploads> FecthCuratedPlaylistObject(string id)
        {
            var response = _service.fetchcuratedobject(id, "playlist");
            if (response == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(response);
            }
        }


        //GET : api/uploads/playlistsave/id   
        [HttpGet("playlistsave/{id}", Name = "FecthPlaylistSave")]
        public ActionResult<Uploads> FecthPlaylistSave(string id)
        {
            var response = _service.fetchcurationsave(id, "playlist");
            if (response == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(response);
            }
        }

        //GET : api/uploads/albumsave/id   
        [HttpGet("albumsave/{id}", Name = "FecthAlbumSave")]
        public ActionResult<Uploads> FecthAlbumSave(string id)
        {
            var response = _service.fetchcurationsave(id, "album");
            if (response == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(response);
            }
        }

        //GET : api/uploads/fetchplayliststream/id  
        [HttpGet("fetchplayliststream/{id}", Name = "FetchPlaylistStream")]
        public ActionResult<PlaylistStreams> FetchPlaylistStream(string id)
        {
            var response = _service.fetchplayliststreambyid(id);
            if (response == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(response);
            }
        }


        //GET : api/uploads/fetchalbumnstream/id  
        [HttpGet("fetchalbumnstream/{id}", Name = "FetchAlbumnStream")]
        public ActionResult<AlbumnStreams> FetchAlbumnStream(string id)
        {
            var response = _service.fetchalbumnstreambyid(id);
            if (response == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(response);
            }
        }


        //GET : api/uploads/fetchsinglestream/id  
        [HttpGet("fetchsinglestream/{id}", Name = "FetchSingleStream")]
        public ActionResult<AlbumnStreams> FetchSingleStream(string id)
        {
            var response = _service.fetchsinglestreambyid(id);
            if (response == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(response);
            }
        }


        //GET : api/uploads/fetchuploadthumbnail?id=id&iscuratedobj=true&type=playlist  
        [HttpGet("fetchuploadthumbnail")]
        public async Task<IActionResult> FetchUploadThumbnail([FromQuery] string id, [FromQuery] bool iscuratedobj, [FromQuery] string type)
        {
            var response = await _service.fetchthumbnails(id, iscuratedobj, type);
            if (response == null)
            {
                return NotFound();
            }
            else
            {

                return File(response.bytes, response.mimetype);
            }
        }


        //GET : api/uploads/streamuploadfile/id 
        [HttpGet("streamuploadfile/{id}")]
        public async Task<IActionResult> StreamUploadFile(string id)
        {
            var response = await _service.streamuploadfile(id);
            if (response == null)
            {
                return NotFound();
            }
            else
            {
                var res = File(response.bytes, response.mimetype);
                res.EnableRangeProcessing = true;
                return res;
            }
        }


        //GET : api/uploads/fetchcuratedsavedobj?id=id&type=playlist    
        [HttpGet("fetchcuratedsavedobj")]
        public ActionResult<List<LookUpCuratedobjsaves>> FetchCuratedSavedObj([FromQuery] string id, [FromQuery] string type)
        {
            List<LookUpCuratedobjsaves> response = _service.fetchsavesforclient(id, type);

            if (response == null)
            {
                return NotFound();
            }
            else
            {

                return Ok(response);
            }
        }


        //GET :  api/uploads/fetchhomedata?region=region&timestamp=timestamp
        [HttpGet("fetchhomedata")]
        public ActionResult<Homefetchresponse> FetchHomeTabData([FromQuery] string region, [FromQuery] int timestamp)
        {

            List<CuratedObjectstream> fetchtop100playlists = _service.fetchtop100playlists();

            List<CuratedObjectstream> fetchtop100playlistsregionfilter = _service.fetchtop100playlistsregionfilter(region);

            List<CuratedObjectstream> fetchtop100playliststimestampfilter = _service.fetchtop100playliststimestampfilter(timestamp);

            List<CuratedObjectstream> fetchtop100playlistsregiontimestampfilter = _service.fetchtop100playlistsregiontimestampfilter(timestamp, region);

            List<Uploadstream> fetchtop100singles = _service.fetchtop100singles();

            List<Uploadstream> fetchtop100singlesregionfilter = _service.fetchtop100singlesregionfilter(region);

            List<Uploadstream> fetchtop100singlestimestampfilter = _service.fetchtop100singlestimestampfilter(timestamp);

            List<Uploadstream> fetchtop100singlesregiontimestampfilter = _service.fetchtop100singlesregiontimestampfilter(region, timestamp);

            List<CuratedObjectstream> fetchtop100albumns = _service.fetchtop100albumns();

            List<CuratedObjectstream> fetchtop100albumnsregionfilter = _service.fetchtop100albumnsregionfilter(region);

            List<CuratedObjectstream> fetchtop100albumnstimestampfilter = _service.fetchtop100albumnstimestampfilter(timestamp);

            List<CuratedObjectstream> fetchtop100albumnsregiontimestampfilter = _service.fetchtop100albumnsregiontimestampfilter(timestamp, region);

            Homefetchresponse response = new Homefetchresponse
            {
                items = new List<Item>
                {
                    new Item{ category = "top 100 playlists" ,type = "playlist" ,iscuratedobject = true , curatedobj = fetchtop100playlists , uploads = null },
                    new Item{ category = "top 100 playlists in " + region ,type = "playlist" , iscuratedobject = true ,  curatedobj = fetchtop100playlistsregionfilter , uploads = null },
                    new Item{ category = "top 100 trending playlists" ,type = "playlist" , iscuratedobject = true ,  curatedobj = fetchtop100playliststimestampfilter, uploads = null },
                    new Item{ category = "top 100 trending playlists in " + region ,type = "playlist"  , iscuratedobject = true ,  curatedobj = fetchtop100playlistsregiontimestampfilter, uploads = null },
                    new Item{ category = "top 100 " , uploads = fetchtop100singles  , iscuratedobject = false ,  curatedobj = null },
                    new Item{ category = "top 100 in " + region  , iscuratedobject = false ,  uploads = fetchtop100singlesregionfilter, curatedobj = null },
                    new Item{ category = "top 100 trending"  , iscuratedobject = false ,  uploads = fetchtop100singlestimestampfilter, curatedobj = null },
                    new Item{ category = "top 100 trending in " + region  , iscuratedobject = false ,  uploads = fetchtop100singlesregiontimestampfilter, curatedobj = null },
                    new Item{ category = "top 100 albumns" ,type = "albumn" , iscuratedobject = true ,  curatedobj = fetchtop100albumns , uploads = null },
                    new Item{ category = "top 100 albumns in " + region,type = "albumn"  , iscuratedobject = true ,  curatedobj = fetchtop100albumnsregionfilter , uploads = null },
                    new Item{ category = "top 100 trending albumns" ,type = "albumn" , iscuratedobject = true ,  curatedobj = fetchtop100albumnstimestampfilter , uploads = null },
                    new Item{ category = "top 100 trending albumns in " + region ,type = "albumn" , iscuratedobject = true ,  curatedobj = fetchtop100albumnsregiontimestampfilter, uploads = null },

                }
            };

            return Ok(response);
        }


        //POST : api/uploads/playliststream   
        [HttpPost("playliststream")]
        public ActionResult<PlaylistStreams> PostPlayListStream(PlaylistStreamRequest request)
        {
            var response = _service.fetchplayliststreambycredentials(request);
            if (response == null)
            {
                var model = _mapper.Map<PlaylistStreams>(request);
                var insert = _service.insertplayliststream(model);
                return CreatedAtRoute(nameof(FetchPlaylistStream), new { Id = insert.Id }, insert);
            }
            else
            {
                var update = _service.updateplayliststream(request, response);
                return CreatedAtRoute(nameof(FetchPlaylistStream), new { Id = response.Id }, response);
            }
        }


        //POST : api/uploads/albumnstream   
        [HttpPost("albumnstream")]
        public ActionResult<PlaylistStreams> PostAlbumnStream(AlbumnStreamRequest request)
        {
            var response = _service.fetchalbumnstreambycredentials(request);
            if (response == null)
            {
                var model = _mapper.Map<AlbumnStreams>(request);
                var insert = _service.insertalbumnstream(model);
                return CreatedAtRoute(nameof(FetchAlbumnStream), new { Id = insert.Id }, insert);
            }
            else
            {
                var update = _service.updatealbumnstream(request, response);
                return CreatedAtRoute(nameof(FetchAlbumnStream), new { Id = response.Id }, response);
            }
        }


        //POST : api/uploads/singlestream   
        [HttpPost("singlestream")]
        public ActionResult<Streams> PostSingleStream(StreamRequest request)
        {
            var response = _service.fetchsinglestreambycredentials(request);
            if (response == null)
            {
                var model = _mapper.Map<Streams>(request);
                var insert = _service.insertsinglestream(model);
                return CreatedAtRoute(nameof(FetchPlaylistStream), new { Id = insert.Id }, insert);
            }
            else
            {
                var update = _service.updatesinglestream(request, response);
                return CreatedAtRoute(nameof(FetchSingleStream), new { Id = response.Id }, response);
            }
        }


        //POST : api/uploads/musicfileupload   
        [HttpPost("musicfileupload")]
        public async Task<ActionResult<Uploads>> UploadMusicFile([FromForm] Uploadmusicrequest requestfile)
        {
            if (!MultipartRequestHelper.IsMultipartContentType(Request.ContentType))
            {
                ModelState.AddModelError("File",
                    $"The request couldn't be processed (Error 1).");
                // Log error
                Console.WriteLine("Not Multipart");
                return BadRequest();
            }
            else
            {
                var model = _mapper.Map<Uploads>(requestfile);
                Console.WriteLine(requestfile.Musicfile);
                var musicext = Path.GetExtension(requestfile.Musicfile.FileName).ToLowerInvariant();
                var thumbnailext = Path.GetExtension(requestfile.Thumbnail.FileName).ToLowerInvariant();
                var musicfilePath = Path.Combine("../public/uploads/",
                        Path.GetRandomFileName());
                var thumbnailPath = Path.Combine("../public/covers/",
                        Path.GetRandomFileName());
                if (requestfile.Musicfile.Length > 0 && requestfile.Thumbnail.Length > 0)
                {
                    if (requestfile.Musicfile.Length > 2097152 && requestfile.Thumbnail.Length > 2097152)
                    {
                        Console.WriteLine("Too big");
                        return BadRequest();
                    }
                    else
                    {
                        if (
                            string.IsNullOrEmpty(thumbnailext) ||
                            !thumbnailExtensions.Contains(thumbnailext) ||
                             string.IsNullOrEmpty(musicext) ||
                            !musicExtensions.Contains(musicext)
                        )
                        {
                            Console.WriteLine("Invalid type " + thumbnailext + " " + musicext);
                        }
                        else
                        {
                            using (var thumbnailstream = System.IO.File.Create(thumbnailPath))
                            using (var musicstream = System.IO.File.Create(musicfilePath))
                            {
                                await requestfile.Musicfile.CopyToAsync(musicstream);
                                await requestfile.Thumbnail.CopyToAsync(thumbnailstream);
                            }
                            Console.WriteLine("Success");
                        }
                    }
                }
                var response = _service.savetodb(model, requestfile.Thumbnail, requestfile.Musicfile, thumbnailext, musicext, thumbnailPath, musicfilePath);
                return CreatedAtRoute(nameof(FecthUpload), new { Id = response.Id }, response);
            }
        }


        //POST : api/uploads/createcuratedobject
        [HttpPost("createcuratedobject")]
        public async Task<ActionResult<Uploads>> UploadMusicFile([FromForm] Createcuratedobj requestobject)
        {
            if (!MultipartRequestHelper.IsMultipartContentType(Request.ContentType))
            {
                ModelState.AddModelError("File",
                    $"The request couldn't be processed (Error 1).");
                // Log error
                Console.WriteLine("Not Multipart");
                return BadRequest();
            }
            else
            {
                var model = _mapper.Map<Curatedobject>(requestobject);
                Console.WriteLine(requestobject.Thumbnail);
                var thumbnailext = Path.GetExtension(requestobject.Thumbnail.FileName).ToLowerInvariant();
                var thumbnailPath = Path.Combine("../public/covers/",
                        Path.GetRandomFileName());
                if (requestobject.Thumbnail.Length > 0)
                {
                    if (requestobject.Thumbnail.Length > 2097152)
                    {
                        Console.WriteLine("Too big");
                        return BadRequest();
                    }
                    else
                    {
                        if (
                            string.IsNullOrEmpty(thumbnailext) ||
                            !thumbnailExtensions.Contains(thumbnailext)
                        )
                        {
                            Console.WriteLine("Invalid type " + thumbnailext);
                        }
                        else
                        {
                            using (var thumbnailstream = System.IO.File.Create(thumbnailPath))
                            {
                                await requestobject.Thumbnail.CopyToAsync(thumbnailstream);
                            }
                            Console.WriteLine("Success");
                        }
                    }
                }
                var response = _service.storeobjindb(model, requestobject.Thumbnail, thumbnailPath, requestobject.Type);
                if (response != null)
                {
                    if (requestobject.Type == "album")
                    {
                        return CreatedAtRoute(nameof(FecthCuratedAlbumObject), new { Id = response.Id }, response);
                    }
                    else
                    {
                        return CreatedAtRoute(nameof(FecthCuratedPlaylistObject), new { Id = response.Id }, response);
                    }
                }
                else
                {
                    return BadRequest();
                }
            }
        }


        //POST: api/uploads/insertintocuratedobj   
        [HttpPost("insertintocuratedobj")]
        public async Task<ActionResult<Curatedobjsaves>> InsertIntoCuratedObj([FromForm] Curatedsavereqest requestfile)
        {
            if (!MultipartRequestHelper.IsMultipartContentType(Request.ContentType))
            {
                ModelState.AddModelError("File",
                    $"The request couldn't be processed (Error 1).");
                // Log error
                Console.WriteLine("Not Multipart");
                return BadRequest();
            }
            else
            {
                var model = _mapper.Map<Uploads>(requestfile);
                Console.WriteLine(requestfile.Musicfile);
                var musicext = Path.GetExtension(requestfile.Musicfile.FileName).ToLowerInvariant();
                var thumbnailext = Path.GetExtension(requestfile.Thumbnail.FileName).ToLowerInvariant();
                var musicfilePath = Path.Combine("../public/uploads/",
                        Path.GetRandomFileName());
                var thumbnailPath = Path.Combine("../public/covers/",
                        Path.GetRandomFileName());
                if (requestfile.Musicfile.Length > 0 && requestfile.Thumbnail.Length > 0)
                {
                    if (requestfile.Musicfile.Length > 2097152 && requestfile.Thumbnail.Length > 2097152)
                    {
                        Console.WriteLine("Too big");
                        return BadRequest();
                    }
                    else
                    {
                        if (
                            string.IsNullOrEmpty(thumbnailext) ||
                            !thumbnailExtensions.Contains(thumbnailext) ||
                             string.IsNullOrEmpty(musicext) ||
                            !musicExtensions.Contains(musicext)
                        )
                        {
                            Console.WriteLine("Invalid type " + thumbnailext + " " + musicext);
                        }
                        else
                        {
                            using (var thumbnailstream = System.IO.File.Create(thumbnailPath))
                            using (var musicstream = System.IO.File.Create(musicfilePath))
                            {
                                await requestfile.Musicfile.CopyToAsync(musicstream);
                                await requestfile.Thumbnail.CopyToAsync(thumbnailstream);
                            }
                            Console.WriteLine("Success");
                        }
                    }
                }
                var response = _service.savetodb(model, requestfile.Thumbnail, requestfile.Musicfile, thumbnailext, musicext, thumbnailPath, musicfilePath);
                var objmodel = _mapper.Map<Curatedobjsaves>(requestfile);
                var objresponse = _service.saveincuration(response.Id, objmodel, requestfile.Type);
                if (requestfile.Type == "album" && objresponse != null)
                {
                    return CreatedAtRoute(nameof(FecthAlbumSave), new { Id = objresponse.Id }, objresponse);
                }
                else if (requestfile.Type == "playlist" && objresponse != null)
                {
                    return CreatedAtRoute(nameof(FecthPlaylistSave), new { Id = objresponse.Id }, objresponse);
                }
                else
                {
                    return BadRequest();
                }
            }
        }






    }
}