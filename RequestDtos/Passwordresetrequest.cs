using System.ComponentModel.DataAnnotations;

namespace infinityrealmapi.RequestDtos
{
    public class PasswordResetRequest
    {
        [Required]
        public string Email{get; set;}  

        [Required]
        public string Password{get; set;}

    }
}