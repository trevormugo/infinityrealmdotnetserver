using System.ComponentModel.DataAnnotations;

namespace infinityrealmapi.RequestDtos
{
    public  class CreateSubscriptionRequestInternal 
    { 
        [Required]   
        public string LocationId{get; set;}
 
        [Required]
        public string PlanId{get; set; }

        [Required]
        public string CustomerId{get; set; }

        [Required]
        public string CardId{get; set; }

        [Required]
        public string Refid{get; set; }

        [Required]
        public string Phonenumber{get; set; }

        [Required]
        public string Email{get; set; }
 
 
    }   
}