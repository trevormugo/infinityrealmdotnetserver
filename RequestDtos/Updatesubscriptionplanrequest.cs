using System.ComponentModel.DataAnnotations;

namespace  infinityrealmapi.RequestDtos
{
    public class Updatesubscriptionplanrequest 
    {
        [Required] 
        public int Amount{get; set;}   

        [Required]   
        public string CountryPlan{get; set;}  

        [Required] 
        public int versionnumber{get; set;}  

    } 
}