using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace infinityrealmapi.RequestDtos
{
    public class Createcuratedobj 
    {

        [Required]
        public string Name{get; set; }

        [Required]
        public IFormFile Thumbnail{get; set; }
        
        [Required]
        public string Curator{get; set; }
        
        [Required]
        public string Type{get; set; }
        
    }
}