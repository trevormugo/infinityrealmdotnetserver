using System.ComponentModel.DataAnnotations;

namespace infinityrealmapi.RequestDtos
{
    public class ChatRepliesRequest
    {

        [Required]
        public string Account { get; set; }

        [Required]
        public string ToAccount { get; set; }

        [Required]
        public string ReplyingToChat { get; set; }

        [Required]
        public string ReplyChatMessage { get; set; }
    }
}