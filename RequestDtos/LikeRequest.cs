using System.ComponentModel.DataAnnotations;

namespace infinityrealmapi.RequestDtos
{
    public class LikeRequest
    {
        [Required]
        public string Account{get; set;}

        [Required]
        public string UploadLiked{get; set;}
    }
}