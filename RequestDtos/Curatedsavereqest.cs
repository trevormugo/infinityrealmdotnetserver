using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace infinityrealmapi.RequestDtos
{
    public class Curatedsavereqest 
    { 
       
        [Required]
        public string Displayname{get; set;}

        [Required]
        public string Uploader{get; set;}

        [Required]
        public bool Downloadable{get; set;}

        [Required]
        public bool Private{get; set;}

        [Required] 
        public IFormFile Musicfile{get; set;}

        [Required] 
        public IFormFile Thumbnail{get; set;}
        
        [Required]
        public string CurationId{get; set; }
        
        [Required]
        public string Type{get; set; }
        

    }
}