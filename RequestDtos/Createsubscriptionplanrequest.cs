using System.ComponentModel.DataAnnotations;

namespace  infinityrealmapi.RequestDtos
{
    public class Createsubscriptionplanrequest 
    {
        [Required] 
        public int Amount{get; set;}   

        [Required]   
        public string CountryPlan{get; set;}  
    } 
}