using System.ComponentModel.DataAnnotations;

namespace infinityrealmapi.RequestDtos
{
    public class Loginrequest
    {
        [Required]
        public string Email{get; set;}

        [Required]
        public string Password{get; set;}
    }
}