using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace infinityrealmapi.RequestDtos
{
    public class AlbumnStreamRequest 
    {

        [Required]
        public string Account{get; set; }
        
        [Required]
        public string Region{get; set; }
        
        [Required]
        public string AlbumnStreamed{get; set; }
        
    }
}