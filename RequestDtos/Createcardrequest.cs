using System.ComponentModel.DataAnnotations;

namespace infinityrealmapi.RequestDtos
{
    public  class Createcardrequest 
    { 
       [Required]   
       public string Cardnonce{get; set;}
 
       [Required]
       public string CustomerId{get; set; }

       [Required]
       public string FullName{get; set; }

       public string VerificationToken{get; set; }

  
    }   
}