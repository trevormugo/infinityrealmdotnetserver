using System.ComponentModel.DataAnnotations;

namespace infinityrealmapi.RequestDtos
{
   public class Signuppostrequest 
   {
       [Required]
       public string Fullname{get; set; }

       [Required]
       public string Username{get; set; }

       [Required] 
       public string Email{get; set; }

       [Required]   
       public string Phonenumber{get; set;}

       [Required]   
       public string Password{get; set;}

   }     
} 