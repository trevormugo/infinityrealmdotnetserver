using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace infinityrealmapi.RequestDtos
{
    public class ChatForwardRequest
    {

        [Required]
        public string Account { get; set; }

        [Required]
        public  List<string> ToAccounts { get; set; }

        [Required]
        public string ChatId { get; set; }

    }
}