using System.ComponentModel.DataAnnotations;

namespace infinityrealmapi.RequestDtos
{
    public  class Createcustomerrequest 
    { 
  
 
       [Required]   
       public string Id{get; set;}
 
       [Required]
       public string Firstnames{get; set; }

       [Required]
       public string Surname{get; set; }


       [Required]
       public string Username{get; set; }

       [Required] 
       public string Email{get; set; }

       [Required]   
       public string Phonenumber{get; set;}

    }   
}