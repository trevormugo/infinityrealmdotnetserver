using System.ComponentModel.DataAnnotations;

namespace infinityrealmapi.RequestDtos
{
    public class ChatRequest
    {

        [Required]
        public string Account { get; set; }

        [Required]
        public string ToAccount { get; set; }

        [Required]
        public string ChatType { get; set; }

        [Required]
        public string ChatMessage { get; set; }
    }
}