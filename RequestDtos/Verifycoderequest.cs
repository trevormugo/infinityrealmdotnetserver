using System.ComponentModel.DataAnnotations;

namespace infinityrealmapi.RequestDtos
{
    public class Verifycoderequest
    {
        [Required]
        public string Email{get; set;}

        [Required] 
        public string Id{get; set;}
    }
}