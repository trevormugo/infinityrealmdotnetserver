using System.ComponentModel.DataAnnotations;

namespace infinityrealmapi.RequestDtos
{
    public class CommentRequest
    {  

        [Required]
        public string Account{get; set; }

        [Required]
        public string UploadCommentedOn{get; set; }

        [Required]
        public string Comment{get; set; }
    }
}