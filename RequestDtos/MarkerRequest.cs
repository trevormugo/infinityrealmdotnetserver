using System.ComponentModel.DataAnnotations;

namespace infinityrealmapi.RequestDtos
{
    public class MarkerRequest
    {
        [Required]
        public string Account{get; set; }

        [Required]
        public double Longitude{get; set; }

        [Required]
        public double Latitude{get; set;}

    }
}