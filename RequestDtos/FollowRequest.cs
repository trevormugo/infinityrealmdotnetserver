using System.ComponentModel.DataAnnotations;

namespace infinityrealmapi.RequestDtos
{
    public class FollowRequest
    {
        [Required]
        public string Account{get; set;}

        [Required]
        public string AccountFollowed{get; set;}
    }
}