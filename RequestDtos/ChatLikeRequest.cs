using System.ComponentModel.DataAnnotations;

namespace infinityrealmapi.RequestDtos
{
    public class ChatLikeRequest
    {

        [Required]
        public string Account { get; set; }

        [Required]
        public string ChatLiked { get; set; }
    }
}