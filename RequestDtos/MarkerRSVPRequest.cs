using System.ComponentModel.DataAnnotations;

namespace infinityrealmapi.RequestDtos
{
    public class MarkerRSVPRequest
    {
        [Required]
        public string Account{get; set; }

        [Required]
        public string Marker{get; set; }

    }
}